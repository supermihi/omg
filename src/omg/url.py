import re

from pathlib import Path


class UrlFormatError(RuntimeError):
    pass


class URL:
    """Identifies a file.

    Each URL is of the form <scheme>://<netloc>/<path>.
    """
    url_expression = re.compile(r'(\w+)://([^/]*)(/.*)')

    path: Path
    netloc: str
    scheme: str

    def __init__(self, url_string: str):
        match = URL.url_expression.match(url_string)
        if match is None or len(match.groups()) != 3:
            raise UrlFormatError(f'unparseable URL: {url_string}')
        self.scheme, self.netloc, path_str = match.groups()
        self.path = Path(path_str)

    def __str__(self):
        return '{}://{}{}'.format(self.scheme, self.netloc, self.path)

    @staticmethod
    def from_local_path(path):
        """Convenience method: create an URL with scheme 'file', and empty netloc.

         Args:
             path: path of the file.
        """
        path = Path(path)
        assert path.is_absolute()
        return URL(f'file://{path}')

    @property
    def extension(self) -> str or None:
        """Returns the lowercased extension of the URL, i.e., part of the path after the final dot, or None if
        if there's no dot in the path."""
        return self.path.suffix.lower()

    def __hash__(self):
        return hash(str(self))

    def __eq__(self, other):
        return str(self) == str(other)

    def __neq__(self, other):
        return str(self) != str(other)

    def __repr__(self):
        return 'URL(\'{}\')'.format(str(self))
