from pathlib import Path

from omg.config import default_config


def test_default_settings():
    root = '/some/path/to/my/music'
    config = default_config(root=root)
    assert config.music_root() == Path(root)
    assert 'flac' in config.track_filetypes()
    assert 'txt' not in config.image_filetypes()
