from pathlib import Path

from confuse import Configuration

ALBUM_FILE_SUFFIX = '.album'


class OmgConfig(Configuration):

    def __init__(self, user=True):
        super().__init__('omg2', __name__, read=False)
        self.read(user=user)

    def track_filetypes(self):
        return self['track_filetypes'].as_str_seq(split=True)

    def image_filetypes(self):
        return self['image_filetypes'].as_str_seq(split=True)

    def db_dir(self) -> Path:
        return Path(self.config_dir())

    def db_type(self) -> Path:
        return self['db_type'].as_choice(('sql', 'dict'))

    def music_root(self) -> Path:
        return Path(self['root'].as_str())


def default_config(**config):
    result = OmgConfig(False)
    result.set(config)
    return result
