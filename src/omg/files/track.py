from dataclasses import dataclass

from omg.model import Flags, Tags
from omg.model.ids import OmgId
from omg.model.pieces import Track
from omg.url import URL


@dataclass(frozen=True)
class TrackData:
    id: OmgId
    flags: Flags
    tags: Tags

    def to_track(self, url: URL):
        return Track(id=self.id, tags=self.tags, flags=self.flags, url=url, covers=[])
