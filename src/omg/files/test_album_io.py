from pathlib import Path
from uuid import UUID

import marshmallow
import pytest

from omg.files.album import AlbumData, ChildRef, CoverRef
from omg.files.album_io import CoverRefSchema, ChildRefSchema, dump_album_to_object, \
    load_album_from_yaml_string, load_album_from_dict
from omg.model.ids import OmgId
from omg.model.pieces import BuiltinAlbumType


def test_load_cover_ref():
    cover_json = {
        'blake': 'abcdef123456',
        'path':  'omg.jpg'
    }

    cover: CoverRef = CoverRefSchema().load(cover_json)
    assert isinstance(cover, CoverRef)
    assert not cover.path.is_absolute()
    assert isinstance(cover.path, Path)
    assert isinstance(cover.blake, str)


def test_wrong_cover_ref():
    cover_json = {'black': '123', 'path': 'omg.jpg'}
    with pytest.raises(marshmallow.ValidationError):
        CoverRefSchema().load(cover_json)


def test_serialize_cover():
    cover = CoverRef(Path('omg.jpg'), 'abcdef123456')
    cover_json = CoverRefSchema().dump(cover)
    expected = {
        'blake': 'abcdef123456',
        'path':  'omg.jpg'
    }
    assert expected == cover_json


def test_load_child_ref():
    child_info_json = {
        'path': '../file1.mp3',
        'id':   str(UUID(int=1))
    }
    schema = ChildRefSchema()
    info: ChildRef = schema.load(child_info_json)

    assert isinstance(info, ChildRef)
    assert info.path == Path('../file1.mp3')
    assert info.id == OmgId.omg(UUID(int=1))


def test_serialize_child_ref():
    info = ChildRef(Path('../file1.mp3'), OmgId.omg(UUID(int=1)))
    schema = ChildRefSchema()
    dumped = schema.dump(info)
    expected = {
        'path': '../file1.mp3',
        'id':   str(UUID(int=1))
    }
    assert expected == dumped


def test_load_album_data():
    album_json = {
        'id':       str(UUID(int=1)),
        'tags':     dict(title='TITLE', artist=['A1', 'A2']),
        'type':     'work',
        'flags':    ['F1', 'F2'],
        'covers':   [{'blake': '123', 'path': 'omg.jpg'}],
        'children': [
            {'path': 'file1.mp3', 'id': str(UUID(int=2))},
            {'path': 'file2.mp3', 'id': str(UUID(int=3))}
        ]
    }
    children = [
        ChildRef(Path('file1.mp3'), OmgId.omg(UUID(int=2))),
        ChildRef(Path('file2.mp3'), OmgId.omg(UUID(int=3)))
    ]
    expected = AlbumData(
        OmgId.omg(UUID(int=1)),
        BuiltinAlbumType.work,
        dict(title=['TITLE'], artist=['A1', 'A2']),
        ['F1', 'F2'],
        [CoverRef(Path('omg.jpg'), '123')],
        children
    )

    album = load_album_from_dict(album_json)
    assert isinstance(album, AlbumData)
    assert expected == album


def test_load_album_data_no_tags_no_flags_no_covers():
    album_json = {
        'id':       str(UUID(int=1)),
        'type':     'work',
        'children': []
    }
    album = load_album_from_dict(album_json)
    assert album.flags == []
    assert album.tags == {}
    assert album.covers == []


def test_dump_album_data():
    children = [
        ChildRef(Path('file1.mp3'), OmgId.omg(UUID(int=2))),
        ChildRef(Path('file2.mp3'), OmgId.omg(UUID(int=3)))
    ]
    album = AlbumData(
        OmgId.omg(UUID(int=1)),
        BuiltinAlbumType.part,
        dict(title=['TITLE'], artist=['A1', 'A2']),
        ['F1', 'F2'],
        [CoverRef(Path('omg.jpg'), 'abcdef123456')],
        children
    )
    album_json = dump_album_to_object(album)
    expected = {
        'id':       str(UUID(int=1)),
        'tags':     dict(title='TITLE', artist=['A1', 'A2']),
        'type':     'part',
        'flags':    ['F1', 'F2'],
        'covers':   [{'blake': 'abcdef123456', 'path': 'omg.jpg'}],
        'children': [
            {'path': 'file1.mp3', 'id': str(UUID(int=2))},
            {'path': 'file2.mp3', 'id': str(UUID(int=3))}
        ]
    }
    assert expected == album_json


def test_load_album_from_yaml():
    file_contents = """\
id: e779d04a-cab3-571f-94de-9fcd2d1b6099
type: album
tags:
  artist: '*shels'
  genre: [Post-Rock, Progressive Rock]
  title: Plains of the Purple Buffalo
flags: [favourite, online]
children:
- {id: b1556f94-6729-5f9f-8aef-4387b75b4fe3, path: 01 - Journey to the Plains.flac}
- {id: f1d1a602-ed32-557c-943d-b9ecd15bf8f3, path: 02 - Plains of the Purple Buffalo
    (Part 1).flac}
- {id: e6a0b350-f53c-5ef4-9991-06e3bc9ccf62, path: 03 - Plains of the Purple Buffalo
    (Part 2).flac}
- {id: 2f661f97-19da-5f00-95bc-459c6fdc730e, path: 04 - Searching for Zihuatanejo.flac}
- {id: 2134da29-9cc1-5c3f-b7b2-07355e4ecf17, path: 05 - Vision Quest.flac}
- {id: f857ea2b-68b7-5fa8-a425-9edc711655c6, path: 06 - Atoll.flac}
- {id: 26847180-01e1-5b09-b377-269026935d03, path: 07 - Butterflies (On Luci's Way).flac}
- {id: 1510bd78-0dac-540e-8e20-4428da8e7e97, path: 08 - Crown of Eagle Feathers.flac}
- {id: 11a1fa07-8d03-505f-9906-7b5614b14c71, path: 09 - Bastien's Angels.flac}
- {id: 7e63b160-98ac-5792-8078-d33fce0a8840, path: 10 - Conqueror.flac}
- {id: 2259b611-18dd-5509-b034-ee52f2f3369e, path: 11 - The Spirit Horse.flac}
- {id: 14c27893-e570-525b-b31a-36e9c8417adb, path: 12 - Waking.flac}
- {id: a5d96d36-50c2-5f48-b9f9-bc0cee937541, path: 13 - Leaving the Plains.flac}
covers:
- {blake: 2c295d1a424357dbbe98813a05eeeb1594e00a8aa9b0e865a5bc7342c97dc5e325a96bcf8ad3d8f10746f0c50c79d2dc8a66ca6eaed77fd7d033819a6a65bd93,
  path: Plains of the Purple Buffalo.png}
"""
    album = load_album_from_yaml_string(file_contents)
    assert album.id == OmgId.omg(UUID('e779d04a-cab3-571f-94de-9fcd2d1b6099'))
    assert album.type == BuiltinAlbumType.album
