import re


def escape_path(path: str):
    return re.sub(r'[^\w\-_. ]', '_', path)
