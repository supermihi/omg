from dataclasses import dataclass, field
from pathlib import Path

from omg.files.tags_io import TagsIO
from omg.model import Tags


@dataclass
class ConstantTagsIO(TagsIO):
    tags: Tags
    supports_id: bool = True
    wrote: bool = field(init=False, default=False)

    def read_tags(self, path: Path) -> Tags:
        return self.tags

    def write_tags(self, tags: Tags, path: Path):
        self.tags = tags
        self.wrote = True

    def supports_id_and_flags(self, path: Path):
        return self.supports_id
