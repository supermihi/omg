from dataclasses import dataclass
from hashlib import blake2b
from pathlib import Path

from omg.model.covers import CoverId, Cover
from omg.url import URL


def get_blake(path: Path) -> CoverId:
    binary = path.read_bytes()
    return blake2b(binary).hexdigest()


def get_proposed_cover_path(piece_path: Path, original_cover_path: Path):
    return piece_path.with_name(f'{piece_path.stem}{original_cover_path.suffix}')


@dataclass(frozen=True)
class CoverData:
    id: CoverId

    def to_cover(self, url: URL):
        return Cover(url=url, id=self.id)


def read_cover_data(path: Path) -> CoverData:
    id = get_blake(path)
    return CoverData(id)
