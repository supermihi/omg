import logging
from pathlib import Path
from typing import Sequence

from marshmallow import Schema, fields, post_load, post_dump

from omg import util
from omg.config import ALBUM_FILE_SUFFIX
from omg.files import escape_path
from omg.files.album import ChildRef, AlbumData, CoverRef
from omg.util import yaml
from omg.util.schemas import PathField, TagsField, OmgIdField

logger = logging.getLogger(__name__)


def propose_album_path(album: AlbumData, children_paths: Sequence[Path]) -> Path:
    name = escape_path(album.tags.get('title', ['album'])[0])
    common_path = util.common_path(children_paths)
    return Path(common_path, f'{name}{ALBUM_FILE_SUFFIX}')


class ChildRefSchema(Schema):
    id = OmgIdField()
    path = PathField()

    @post_load
    def make_child_ref(self, data, **kwargs):
        return ChildRef(data['path'], data['id'])


class CoverRefSchema(Schema):
    blake = fields.Str()
    path = PathField()

    @post_load
    def make_cover_ref(self, data, **kwargs):
        return CoverRef(**data)


class AlbumFileSchema(Schema):
    id = OmgIdField()
    type = fields.String()
    tags = TagsField(missing=dict)
    flags = fields.List(fields.Str(), missing=list)

    covers = fields.List(fields.Nested(CoverRefSchema), missing=list)
    children = fields.List(fields.Nested(ChildRefSchema), missing=list)

    class Meta:
        ordered = True

    @post_dump()
    def remove_empty_lists(self, data, **kwargs):
        if not data['covers']:
            del data['covers']
        if not data['flags']:
            del data['flags']
        return data

    @post_load()
    def make_albumfile_info(self, data, **kwargs):
        return AlbumData(data['id'], data['type'], data['tags'], data['flags'], data['covers'], data['children'])


_album_schema = AlbumFileSchema()


def dump_album_to_object(info: AlbumData):
    return _album_schema.dump(info)


def load_album_from_dict(obj: dict) -> AlbumData:
    return _album_schema.load(obj)


def load_album_from_yaml_string(content: str) -> AlbumData:
    yaml_album = yaml.load(content)
    return load_album_from_dict(yaml_album)


def read_album_data(path: Path) -> AlbumData:
    if not path.suffix:
        path = path.with_suffix(ALBUM_FILE_SUFFIX)
    text = path.read_text()
    return load_album_from_yaml_string(text)


def dump_album_to_file(album: AlbumData, path: Path):
    if not path.suffix:
        path = path.with_suffix(ALBUM_FILE_SUFFIX)
    obj = dump_album_to_object(album)
    logger.info(f'writing {album} to {path}')
    with path.open('wt') as stream:
        yaml.dump(obj, stream)
