from abc import ABC, abstractmethod
from os import fspath
from pathlib import Path

import taglib

from omg.model import Tags


def normalize_tags(tags: Tags) -> Tags:
    return {tag.lower(): values for tag, values in tags.items()}


class TagWriteError(Exception):
    pass


class TagsIO(ABC):

    @abstractmethod
    def read_tags(self, path: Path) -> Tags:
        raise NotImplementedError()

    def write_tags(self, tags: Tags, path: Path):
        raise NotImplementedError()

    def supports_id_and_flags(self, path: Path):
        return True


class TaglibTagsIO(TagsIO):

    def read_tags(self, path: Path) -> Tags:
        taglib_file = taglib.File(fspath(path))
        file_tags = taglib_file.tags
        return normalize_tags(file_tags)

    def write_tags(self, tags: Tags, path: Path):
        taglib_file = taglib.File(str(path))
        tags = normalize_tags(tags)
        taglib_file.tags = tags
        unsaved = taglib_file.save()
        if len(unsaved) > 0:
            raise TagWriteError(f'unsupported tags not saved to {path}: {",".join(unsaved.keys())}')

    def supports_id_and_flags(self, path: Path):
        return path.suffix.lower() not in ('.wma', '.m4a')
