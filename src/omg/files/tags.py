from pathlib import Path
from typing import Tuple
import uuid
import logging
from uuid import UUID

from omg.files.tags_io import TagsIO
from omg.files.track import TrackData
from omg.model import Tags, Flags
from omg.model.ids import create_omg_id, OmgId
from omg.util.tags import get_tags_except

"""Module concerned with representing OMG data (id, flags, tags) in plain audio metadata tags."""

ID_TAG = 'omg_id'
FLAGS_TAG = 'flags'

log = logging.getLogger(__name__)


class TagFormatError(Exception):
    pass


class IdTagError(TagFormatError):
    pass


SUFFIXES_NOT_SUPPORTING_ID = {'.wma', '.m4a'}


def supports_id_tag(path: Path):
    return path.suffix.lower() not in SUFFIXES_NOT_SUPPORTING_ID


def read_track_data(path: Path, io: TagsIO) -> TrackData:
    read_tags = io.read_tags(path)
    return ensure_has_in_track_data(read_tags, path, io)


def extract_in_track_data(file_tags: Tags) -> TrackData:
    id, tags = extract_id(file_tags)
    flags, tags = extract_flags(tags)
    return TrackData(id, flags, tags)


def save_in_track_data(path: Path, data: TrackData, io: TagsIO):
    tags = encode_in_track_data(data)
    io.write_tags(tags, path)


def encode_in_track_data(data: TrackData):
    tags = dict(data.tags)
    tags[ID_TAG] = [str(data.id)]
    if len(data.flags) > 0:
        tags[FLAGS_TAG] = data.flags
    return tags


def extract_flags(tags: Tags) -> Tuple[Flags, Tags]:
    flags = get_flags(tags)
    remaining_tags = get_tags_except(tags, [FLAGS_TAG])
    return flags, remaining_tags


def get_flags(tags: Tags) -> Flags:
    flags = tags.get(FLAGS_TAG, None)
    return [] if flags is None else flags


def extract_id(tags: Tags) -> Tuple[OmgId, Tags]:
    id = get_id(tags)
    remaining_tags = get_tags_except(tags, [ID_TAG])
    return id, remaining_tags


def has_id(tags: Tags) -> bool:
    try:
        get_id(tags)
        return True
    except:
        return False


def get_id(tags: Tags) -> OmgId:
    tag_value = try_get_id_tag(tags)
    try:
        uuid = UUID(tag_value)
    except ValueError:
        raise IdTagError(f'malformed UUID {tag_value}')
    return OmgId.omg(uuid)


def try_get_id_tag(tags: Tags) -> str:
    if ID_TAG not in tags:
        raise IdTagError(f'no {ID_TAG} tag')
    values = tags[ID_TAG]
    if len(values) != 1:
        raise IdTagError(f'non-singular {ID_TAG} tag')
    return values[0]


def add_id(path: Path, tags: Tags):
    id = create_omg_id(path)
    return dict(tags, ID_TAG=[str(id)])


def get_path_based_id(path: Path):
    return OmgId.omg(uuid.uuid5(uuid.NAMESPACE_URL, str(path)))


def augment_artificial_id(tags: Tags, path: Path):
    log.warning(f'creating path-based id for unsupported file: {path}')
    id = get_path_based_id(path)
    return TrackData(id, [], tags)


def ensure_has_in_track_data(tags: Tags, path: Path, io: TagsIO) -> TrackData:
    if not io.supports_id_and_flags(path):
        return augment_artificial_id(tags, path)
    try:
        return extract_in_track_data(tags)
    except IdTagError:
        flags, tags = extract_flags(tags)
        new_data = TrackData(create_omg_id(path), flags, tags)
        save_in_track_data(path, new_data, io)
        return new_data
