from omg.files import escape_path


def test_escape_path():
    assert escape_path('omg wtf') == 'omg wtf'
    assert escape_path('some/path.mp3') == 'some_path.mp3'
    assert escape_path('w?t:f*') == 'w_t_f_'
