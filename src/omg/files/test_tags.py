from pathlib import Path
from uuid import UUID

from omg.files import tags as tags_module
from omg.files.tags import ensure_has_in_track_data, has_id, ID_TAG, get_path_based_id
from omg.files.track import TrackData
from omg.files.constant_tags_io import ConstantTagsIO
from omg.model.ids import create_omg_id, OmgId


def test_extract_in_track_data():
    file_tags = {
        'title':  ['one', 'two'],
        'artist': ['art'],
        'omg_id': [str(UUID(int=42))],
        'flags':  ['fl1', 'fl2']
    }

    data = tags_module.extract_in_track_data(file_tags)
    assert data.id == OmgId.omg(UUID(int=42))
    assert data.flags == ['fl1', 'fl2']
    assert len(data.tags) == 2
    assert 'title' in data.tags
    assert 'artist' in data.tags


def test_encode_in_track_data():
    id = OmgId.omg(UUID(int=42))
    flags = ['fl1', 'fl2']
    tags = {'title': ['Tit1']}
    data = TrackData(id, flags, tags)

    file_tags = tags_module.encode_in_track_data(data)
    assert file_tags == {
        'omg_id': [str(UUID(int=42))],
        'flags':  ['fl1', 'fl2'],
        'title':  ['Tit1']
    }


def test_ensure_in_track_data_adds_id():
    tags = {'no_id': ['this is no id']}
    io = ConstantTagsIO(tags)
    ensure_has_in_track_data(io.tags, Path(), io)
    assert has_id(io.tags)


def test_ensure_in_track_data_does_not_touch_tags_with_id():
    tags = {ID_TAG: [str(create_omg_id(Path('/test')))]}
    io = ConstantTagsIO(tags)
    ensure_has_in_track_data(io.tags, Path(), io)
    assert tags is io.tags
    assert not io.wrote


def test_ensure_in_track_data_returns_when_id_not_supported():
    tags = {'author': ['Me']}
    path = Path('/a/b/c.wma')
    io = ConstantTagsIO(tags, supports_id=False)
    result = ensure_has_in_track_data(tags, path, io)
    assert not io.wrote
    assert result.id == get_path_based_id(path)
