import logging
from dataclasses import dataclass
from pathlib import PurePath, Path
from typing import Sequence

from omg.model import Tags, Flags
from omg.model.covers import CoverId
from omg.model.ids import OmgId
from omg.model.pieces import PieceOfMusic, Album
from omg.url import URL

logger = logging.getLogger('files.album')


@dataclass(frozen=True)
class ChildRef:
    path: PurePath
    id: OmgId

    def __post_init__(self):
        assert not self.path.is_absolute()

    def __str__(self):
        return repr(self)


@dataclass(frozen=True)
class CoverRef:
    """Cover reference in an album file."""
    path: Path
    blake: CoverId

    def __post_init__(self):
        assert not self.path.is_absolute()

    def __str__(self):
        return repr(self)


@dataclass(frozen=True)
class AlbumData:
    """Represents the information about an album (usually stored in an .album file).
    """
    id: OmgId
    type: str
    tags: Tags
    flags: Flags
    covers: Sequence[CoverRef]
    children: Sequence[ChildRef]

    def display_name(self):
        if 'title' in self.tags:
            return self.tags['title'][0]
        return str(self.id)

    def to_album(self, url: URL):
        return Album(id=self.id, type=self.type, tags=self.tags, flags=self.flags, url=url,
                     covers=[c.blake for c in self.covers],
                     children=[c.id for c in self.children])
    def __str__(self):
        return f'Album({self.display_name()})'


def album_to_albumdata(album: Album, children: Sequence[PieceOfMusic], cover_paths: Sequence[Path],
                       path: Path) -> AlbumData:
    directory = path.parent
    child_infos = [ChildRef(c.url.path.relative_to(directory), c.id) for c in children]
    cover_infos = [CoverRef(p.relative_to(directory), blake) for blake, p in zip(album.covers, cover_paths)]
    return AlbumData(album.id, album.type, album.tags, album.flags, cover_infos, child_infos)
