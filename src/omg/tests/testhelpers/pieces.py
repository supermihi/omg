from pathlib import Path
from uuid import uuid4

from omg.model.ids import OmgId
from omg.model.pieces import Track
from omg.url import URL


def mock_track(**kwargs) -> Track:
    id = kwargs.get('id', OmgId.omg(uuid4()))
    tags = kwargs.get('tags', {})
    flags = kwargs.get('flags', [])
    url = get_url(kwargs.get('url'))
    covers = kwargs.get('covers', [])
    return Track(id=id, tags=tags, flags=flags, url=url, covers=covers)


def get_url(url_arg):
    if url_arg is None:
        return URL.from_local_path(Path('/test.mp3'))
    if isinstance(url_arg, URL):
        return url_arg
    if isinstance(url_arg, str):
        return URL.from_local_path(Path(url_arg))
    return URL.from_local_path(url_arg)
