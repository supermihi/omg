from abc import ABCMeta, abstractmethod
from dataclasses import dataclass
from typing import Iterable, List, Union, Sequence, Optional, Tuple, Collection

from omg.config import OmgConfig
from omg.model.covers import CoverId, Cover
from omg.model.ids import OmgId
from omg.model.pieces import PieceOfMusic, PieceData
from omg.url import URL

TagAndValue = Tuple[str, str]
TagGroup = Iterable[TagAndValue]


class DatabaseConsistencyError(RuntimeError):
    """Thrown when the database is found in, or being modified into, an inconsistent state."""
    pass


DisjunctiveTagClause = Collection[TagAndValue]
TagQuery = Collection[DisjunctiveTagClause]


class MusicDatabase(metaclass=ABCMeta):
    """Interface for OMG database access."""

    @abstractmethod
    def get_piece(self, id: OmgId) -> PieceOfMusic:
        """Returns a single piece of music with the given OmgId."""
        pass

    @abstractmethod
    def lookup(self, url: URL) -> Optional[PieceOfMusic]:
        """Lookup a piece by its URL. Returns None if no such piece is known."""
        pass

    @abstractmethod
    def get_pieces(self, ids: Iterable[OmgId] = None, data: Sequence[str] = None) -> List[PieceData]:
        """
        Returns all piece data for the given fields. All pieces if no ids are given.
        """
        pass

    @abstractmethod
    def has_piece(self, id: OmgId) -> bool:
        pass

    @abstractmethod
    def add_pieces(self, pieces: Union[PieceOfMusic, Sequence[PieceOfMusic]]) -> None:
        """
        Add pieces to the database.
        """
        pass

    @abstractmethod
    def match_tags(self, query: str, tags: Collection[str] = None) -> List[TagAndValue]:
        """
        Find tag/value combinations.
        """

    @abstractmethod
    def delete_piece(self, id: Union[OmgId, URL]):
        """
        Deletes the given piece.
        """
        pass

    @abstractmethod
    def update_piece(self, piece: PieceOfMusic):
        """
        Update the information of the given piece.
        """
        pass

    @abstractmethod
    def get_cover(self, url: URL) -> Cover:
        """
        Get the cover for the given URL.
        """
        pass

    @abstractmethod
    def get_cover_by_id(self, id: CoverId) -> Cover:
        """
        Get one cover for the given id.
        """
        pass

    @abstractmethod
    def add_cover(self, cover: Cover):
        """
        Register a cover file to the database.
        """
        pass

    @abstractmethod
    def get_covers(self) -> Sequence[Cover]:
        """Return all covers."""
        pass

    @abstractmethod
    def update_cover(self, cover: Cover):
        raise NotImplementedError()

    @abstractmethod
    def remove_cover(self, url: URL):
        raise NotImplementedError()

    def finalize(self):
        pass

    @abstractmethod
    def get_pieces_for_tags(self, query: TagQuery, reduce_tree: bool = True, data: Sequence[str] = None) -> Iterable[
        PieceData]:
        pass


DEFAULT_SQLITE_FILENAME = 'omg_db.sqlite'


def create_default(config: OmgConfig, db_loglevel=None) -> MusicDatabase:
    if config.db_type() == 'dict':
        from omg.database.dict import FileBasedDictMusicDatabase
        return FileBasedDictMusicDatabase(config.db_dir() / 'omg.db')
    elif config.db_type() == 'sql':
        from omg.database.peewee.peeweedb import PeeweeDatabase, initialize_db, create_tables
        path = config.db_dir() / DEFAULT_SQLITE_FILENAME
        db = initialize_db(path)
        if not path.exists():
            db.connect()
        create_tables()
        if db_loglevel is not None:
            import logging
            logger = logging.getLogger('peewee')
            logger.setLevel(db_loglevel)
        return PeeweeDatabase()
