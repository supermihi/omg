from datetime import datetime, timezone
from itertools import groupby
from pathlib import Path
from typing import Iterable, Union, List, Sequence, Optional, Collection, Sized
import logging
from peewee import *

from omg.database import MusicDatabase, TagAndValue, TagQuery
from omg.model import Tags, Flags
from omg.model.covers import CoverId, Cover as CoverModel
from omg.model.ids import OmgId
from omg.model.pieces import PieceOfMusic, Track, Album, PieceData
from omg.sync.db import SyncDatabase
from omg.url import URL
from omg.util.tags import add_tag_to
from omg.util.time import min_utc

log = logging.getLogger(__name__)
db = SqliteDatabase(None)


def initialize_db(db_path: Path = None):
    db.init(db_path or ':memory:', pragmas={'foreign_keys': 1})
    return db


class BaseModel(Model):
    class Meta:
        database = db


class Piece(BaseModel):
    id = CharField(primary_key=True)
    url = CharField(max_length=1023, index=True)
    is_album = BooleanField()
    album_type = CharField(null=True)

    def omg_id(self) -> OmgId:
        return OmgId.from_string(self.id)

    def omg_url(self) -> URL:
        return URL(self.url)

    def children(self):
        rels = (ChildrenOfPiece
                .select(ChildrenOfPiece.child)
                .where(ChildrenOfPiece.parent == self)
                .order_by(ChildrenOfPiece.position.asc()))
        return [cr.child_omg_id() for cr in rels]

    def tags(self) -> Tags:
        rels = (TagsOfPiece
                .select(Tag.name, TagValue.value).where(TagsOfPiece.piece == self)
                .join(TagValue, on=TagsOfPiece.value)
                .join(Tag, on=TagValue.tag)
                )
        return {key: list(e.value.value for e in group) for key, group in groupby(rels, lambda d: d.value.tag.name)}

    def flags(self) -> Flags:
        return [fop.flag.name for fop in FlagsOfPiece.select(Flag.name).where(FlagsOfPiece.piece == self).join(Flag)]

    def covers(self) -> Sequence[CoverId]:
        return [cop.cover for cop in CoverOfPiece.select().where(CoverOfPiece.piece == self)]

    def as_omg_piece(self) -> PieceOfMusic:
        tags = self.tags()
        flags = self.flags()
        covers = self.covers()
        if self.is_album:
            return Album(self.omg_id(), tags, flags, self.omg_url(), covers, self.children(), self.album_type)
        else:
            return Track(self.omg_id(), tags, flags, self.omg_url(), covers)

    @staticmethod
    def add_piece(piece: PieceOfMusic):
        with db.atomic():
            if isinstance(piece, Album):
                db_piece = Piece.create(id=str(piece.id), url=str(piece.url), is_album=True, album_type=piece.type)
                ChildrenOfPiece.add_child_refs(db_piece, piece.children)
            else:
                db_piece = Piece.create(id=str(piece.id), url=str(piece.url), is_album=False)
            TagsOfPiece.add_tags(db_piece, piece.tags)
            FlagsOfPiece.add_flags(db_piece, piece.flags)
            CoverOfPiece.add_covers(db_piece, piece.covers)


class ChildrenOfPiece(BaseModel):
    parent = ForeignKeyField(Piece, index=True)
    child = CharField(index=True)
    position = IntegerField()

    class Meta:
        indexes = (
            (('parent', 'position'), True),
        )

    def child_omg_id(self):
        return OmgId.from_string(self.child)

    @staticmethod
    def add_child_refs(piece: Piece, children: Sequence[OmgId]):
        ChildrenOfPiece.insert_many([(piece.id, child_id, pos) for pos, child_id in enumerate(children)]).execute()


class Tag(BaseModel):
    name = CharField(primary_key=True, index=True)


class TagValue(BaseModel):
    value = CharField(index=True)
    tag = ForeignKeyField(Tag, backref='values', index=True)
    sort_value = CharField(null=True)

    class Meta:
        indexes = (
            (('value', 'tag'), True),
        )


class TagsOfPiece(BaseModel):
    piece = ForeignKeyField(Piece, index=True, backref='tags_of_piece')
    value = ForeignKeyField(TagValue, index=True)

    @staticmethod
    def add_tags(piece: Piece, tags: Tags):
        tags_of_piece_adds = []
        for tag, values in tags.items():
            db_tag, _ = Tag.get_or_create(name=tag)
            for value in values:
                db_value, _ = TagValue.get_or_create(value=value, tag=db_tag.name)
                tags_of_piece_adds.append((piece.id, db_value.id))
        TagsOfPiece.insert_many(tags_of_piece_adds, fields=(TagsOfPiece.piece, TagsOfPiece.value)).execute()


def query_piece_ids_for_tags(query: TagQuery) -> Collection[Piece]:
    clause_queries = []
    for clause in query:
        tags_and_values = [f'{tag}{value}' for tag, value in clause]
        existing_vals = TagValue.select(TagValue.id).where(TagValue.tag_id.concat(TagValue.value).in_(tags_and_values))
        pieces_of_clause = TagsOfPiece.select(TagsOfPiece.piece_id).where(
            TagsOfPiece.value.in_(existing_vals))
        clause_queries.append(pieces_of_clause)
    ans = clause_queries[0]
    for cq in clause_queries[1:]:
        ans = ans & cq
    return ans


class Flag(BaseModel):
    name = CharField(primary_key=True)


class FlagsOfPiece(BaseModel):
    piece = ForeignKeyField(Piece, index=True)
    flag = ForeignKeyField(Flag, index=True)

    @staticmethod
    def add_flags(piece: Piece, flags: Flags):
        for flag in flags:
            db_flag, new = Flag.get_or_create(name=flag)
            FlagsOfPiece.create(piece=piece, flag=db_flag)


class Cover(BaseModel):
    hash = CharField(index=True)
    url = CharField(max_length=1023, index=True)

    def as_omg_cover(self):
        return CoverModel(url=URL(self.url), id=self.hash)


class CoverOfPiece(BaseModel):
    piece = ForeignKeyField(Piece, index=True)
    cover = CharField(index=True)

    @staticmethod
    def add_covers(piece: Piece, covers: Sequence[CoverId]):
        CoverOfPiece.insert_many(({'piece': piece.id, 'cover': cover_id} for cover_id in covers)).execute()


class Sync(BaseModel):
    path = CharField(max_length=1023, primary_key=True)
    last_check = TimestampField(utc=True)

    @property
    def last_check_utc(self):
        return self.last_check.replace(tzinfo=timezone.utc)


def create_tables():
    from omg.database.peewee.search import Parents
    models = [Piece, ChildrenOfPiece, Tag, TagValue, TagsOfPiece, Flag, FlagsOfPiece, Cover, CoverOfPiece, Sync,
              Parents]
    db.create_tables(models)


class PeeweeDatabase(MusicDatabase, SyncDatabase):

    def __init__(self):
        log.info(
            f'loading sqlite database with {Piece.select().count()} pieces and {TagsOfPiece.select().count()} tags')

    def update_aux_tables(self):
        from omg.database.peewee.search import Parents
        Parents.create()

    def get_piece(self, id: OmgId) -> PieceOfMusic:
        return Piece.get_by_id(str(id)).as_omg_piece()

    def lookup(self, url: URL) -> Optional[PieceOfMusic]:
        piece = Piece.get_or_none(Piece.url == str(url))
        return None if piece is None else piece.as_omg_piece()

    def get_pieces(self, ids: Iterable[OmgId] = None, data: Sequence[str] = None) -> List[PieceData]:
        query = Piece.select(Piece.id)
        if ids is not None:
            query = query.where(Piece.id in [str(id) for id in ids])
        pieces = self._get_pieces_from_ids(query, data)
        if ids is not None:
            piece_by_id = {piece.id: piece for piece in pieces}
            return [piece_by_id[id] for id in ids]
        return pieces

    def _get_pieces_from_ids(self, ids, data: Sequence[str] = None):
        if data is None:
            data = set()
        if 'flags' in data or 'children' in data or 'covers' in data:
            raise NotImplementedError()

        pieces = Piece.select().where(Piece.id.in_(ids))
        if 'tags' in data:
            pieces = pieces.prefetch(TagsOfPiece, TagValue, Tag)

        def get_tags(piece):
            if 'tags' in data:
                tags = {}
                for top in piece.tags_of_piece:
                    add_tag_to(tags, top.value.tag.name, [top.value.value])
                return tags
            return None

        return [PieceData(p.omg_id(), url=p.omg_url(), is_album=p.is_album, album_type=p.album_type,
                          tags=get_tags(p)) for p in pieces]

    def has_piece(self, id: OmgId) -> bool:
        return Piece.select().where(Piece.id == str(id)).count() == 1

    def add_pieces(self, pieces: Union[Sequence[PieceOfMusic], PieceOfMusic]) -> None:
        if isinstance(pieces, PieceOfMusic):
            pieces = [pieces]
        for piece in pieces:
            Piece.add_piece(piece)

    def match_tags(self, query: str, tags: Collection[str] = None) -> List[TagAndValue]:
        db_query = TagValue.select().where(TagValue.value.contains(query))
        if tags is not None:
            db_query = db_query.where(TagValue.tag.in_(tags))
        return [(tv.tag_id, tv.value) for tv in db_query]

    def delete_piece(self, id: Union[OmgId, URL]):
        raise NotImplementedError()

    def update_piece(self, piece: PieceOfMusic):
        raise NotImplementedError()

    def get_pieces_for_tags(self, query: TagQuery, reduce: bool = True, data: Sequence[str] = None) -> Iterable[
        PieceData]:
        from omg.database.peewee.search import reduce_piece_tree
        piece_ids = query_piece_ids_for_tags(query)
        if reduce:
            piece_ids = reduce_piece_tree(piece_ids)
        return self._get_pieces_from_ids(piece_ids, data)

    def get_cover(self, url: URL) -> CoverModel:
        return Cover.get(Cover.url == str(url)).as_omg_cover()

    def get_cover_by_id(self, id: CoverId) -> CoverModel:
        return Cover.get_by_id(id).as_omg_cover()

    def add_cover(self, cover: CoverModel):
        Cover.create(hash=cover.id, url=str(cover.url))

    def get_covers(self) -> Sequence[CoverModel]:
        return [c.as_omg_cover() for c in Cover.select()]

    def update_cover(self, cover: CoverModel):
        db_cover = Cover.get(Cover.url == str(cover.url))
        db_cover.hash = cover.id
        db_cover.save()

    def remove_cover(self, url: URL):
        Cover.get(Cover.url == str(url)).delete_instance()

    def last_check(self, path: Path) -> datetime:
        ans = Sync.get_or_none(Sync.path == str(path))
        return ans.last_check_utc if ans is not None else min_utc()

    def set_last_check(self, path: Path, checked: datetime):
        Sync.insert(path=str(path), last_check=checked).on_conflict(
            conflict_target=Sync.path,
            preserve=[Sync.last_check]
        )
        Sync.create(path=str(path), last_check=checked)

    def delete_path(self, path: Path):
        entry = Sync.get(Sync.path == str(path))
        entry.delete_instance()
