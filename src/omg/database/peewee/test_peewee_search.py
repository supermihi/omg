from omg.database.peewee.peeweedb import query_piece_ids_for_tags
from omg.database.peewee.search import Parents
from omg.model.ids import OmgId
from omg.model.pieces import Album, BuiltinAlbumType
from omg.tests.testhelpers.pieces import mock_track
from omg.url import URL


def test_parents_table_contains_2nd_level(peewee_db):
    track = mock_track()

    album = Album(OmgId.random(), {'Artist': ['Leonard Cohen']}, ['favorite'], URL.from_local_path('/a/b/c.album'),
                  ['13456'], [track.id], BuiltinAlbumType.album)
    collection = Album(OmgId.random(), {'Artist': ['Leonard Cohen'], 'title': ['Collection']}, [],
                       URL.from_local_path('/a/b/collection.album'), [], [album.id], BuiltinAlbumType.collection)
    peewee_db.add_pieces([track, collection, album])
    Parents.create()
    parents_of_track = list(Parents.select().where(Parents.piece == str(track.id)).order_by(Parents.steps))
    assert parents_of_track[0].parent_id == str(album.id)
    assert parents_of_track[0].steps == 1
    assert parents_of_track[1].parent_id == str(collection.id)
    assert parents_of_track[1].steps == 2


def test_query_piece_ids_for_tags(peewee_db):
    track = mock_track(tags={'artist': ['dream theater'], 'title': ['peruvian skies']})
    peewee_db.add_pieces(track)
    clauses = [
        [[('artist', 'dream theater')]],
        [[('artist', 'dream theater'), ('artist', 'bob dylan')]],
        [[('artist', 'dream theater')], [('title', 'peruvian skies')]],
    ]
    clauses_not_matching = [
        [[('artist', 'bob dylan')]],
        [[('artist', 'dream theater')], [('title', 'blowin in the wind')]],

    ]
    for clause in clauses:
        assert len(query_piece_ids_for_tags(clause)) == 1
    for clause in clauses_not_matching:
        assert len(query_piece_ids_for_tags(clause)) == 0


def test_get_pieces_for_tags_with_reduction(peewee_db):
    track = mock_track(tags={'artist': ['dream theater'], 'title': ['peruvian skies']})
    album = Album(OmgId.random(), {'artist': ['dream theater'], 'title': ['falling into infinity']}, [],
                  URL.from_local_path('/test.album'), [], [track.id], BuiltinAlbumType.album)
    peewee_db.add_pieces([track, album])
    peewee_db.update_aux_tables()
    pieces = list(peewee_db.get_pieces_for_tags([[('artist', 'dream theater')]], True))
    assert len(pieces) == 1
    assert peewee_db.get_piece(pieces[0].id) == album


def test_get_pieces_for_tags_without_reduction(peewee_db):
    track = mock_track(tags={'artist': ['dream theater'], 'title': ['peruvian skies']})
    album = Album(OmgId.random(), {'artist': ['dream theater'], 'title': ['falling into infinity']}, [],
                  URL.from_local_path('/test.album'), [], [track.id], BuiltinAlbumType.album)
    peewee_db.add_pieces([track, album])
    peewee_db.update_aux_tables()
    piece_datas = list(peewee_db.get_pieces_for_tags([[('artist', 'dream theater')]], False))
    piece_ids = [d.id for d in piece_datas]
    assert len(piece_ids) == 2
    assert set(piece_ids) == {track.id, album.id}
