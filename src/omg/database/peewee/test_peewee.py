from datetime import datetime, timezone
from pathlib import Path

from peewee import Tuple, ValuesList, Value, AsIs
from playhouse.test_utils import assert_query_count

from omg.database.peewee.peeweedb import TagValue
from omg.model.ids import OmgId
from omg.model.pieces import Track, Album, BuiltinAlbumType
from omg.tests.testhelpers.pieces import mock_track
from omg.url import URL
from omg.util.time import min_utc


def test_retrieved_track_equals_inserted(peewee_db):
    id = OmgId.random()
    piece = Track(id, {'Artist': ['Dream Theater']}, ['ripped'], URL.from_local_path('/omg.mp3'), [])

    peewee_db.add_pieces([piece])
    piece_from_db = peewee_db.get_piece(id)
    assert piece_from_db == piece


def test_retrieved_album_equals_inserted(peewee_db):
    tracks = [mock_track() for _ in range(5)]
    peewee_db.add_pieces(tracks)
    album = Album(OmgId.random(), {'Artist': ['Leonard Cohen']}, ['favorite'], URL.from_local_path('/a/b/c.album'),
                  ['13456'], [t.id for t in tracks], BuiltinAlbumType.album)
    peewee_db.add_pieces(album)
    album_from_db = peewee_db.get_piece(album.id)
    assert album_from_db == album


def test_sync_db(peewee_db):
    assert peewee_db.last_check(Path('/wont/exist')) == min_utc()
    peewee_db.set_last_check(Path('/exists'), datetime(2020, 1, 21, 19, 14, 0))
    lc = peewee_db.last_check(Path('/exists'))
    assert lc == datetime(2020, 1, 21, 19, 14, 0).replace(tzinfo=timezone.utc)


def test_get_tags_prefetch(peewee_db):
    piece_1 = mock_track(tags={'artist': ['Artist A', 'Artist B'], 'album': ['Album A']})
    piece_2 = mock_track(tags={'artist': ['Artist A'], 'title': ['Title 2']})
    peewee_db.add_pieces([piece_1, piece_2])

    with assert_query_count(4):
        pieces = peewee_db.get_pieces([piece_1.id, piece_2.id], data=['tags'])
    assert pieces[0].tags == piece_1.tags
    assert pieces[1].tags == piece_2.tags
