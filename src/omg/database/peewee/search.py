from itertools import count

from peewee import *

from omg.database.peewee.peeweedb import BaseModel, Piece, ChildrenOfPiece, db, TagsOfPiece, TagValue


class Parents(BaseModel):
    piece = ForeignKeyField(Piece)
    parent = ForeignKeyField(Piece)
    steps = IntegerField(null=True)

    @staticmethod
    def create():
        Parents.delete().execute()
        children = ChildrenOfPiece.select(ChildrenOfPiece.child, ChildrenOfPiece.parent, 1).where(
            ChildrenOfPiece.child.in_(Piece.select(Piece.id)))
        Parents.insert_from(children, [Parents.piece, Parents.parent, Parents.steps]).execute()
        for steps in count(2):
            source = (Parents
                      .select(Parents.piece, ChildrenOfPiece.parent, steps)
                      .where(Parents.steps == steps - 1)
                      .join(ChildrenOfPiece, on=(ChildrenOfPiece.child == Parents.parent))
                      )
            affected = Parents.insert_from(source, [Parents.piece, Parents.parent, Parents.steps]).execute()
            if affected == 0:
                break


def reduce_piece_tree(piece_ids):
    pieces_with_parents = Parents.select(Parents.piece).where(Parents.piece.in_(piece_ids) & Parents.parent.in_(piece_ids))
    return piece_ids.except_(pieces_with_parents)
