from logging import StreamHandler

import pytest
import logging


@pytest.fixture
def peewee_db():
    logger = logging.getLogger('peewee')
    #logger.addHandler(StreamHandler())
    #logger.setLevel(logging.DEBUG)
    from omg.database.peewee.peeweedb import PeeweeDatabase, create_tables, initialize_db
    db = initialize_db()
    db.connect()
    create_tables()
    return PeeweeDatabase()
