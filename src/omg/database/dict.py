import logging
import pickle
import re
from pathlib import Path
from typing import Iterable, List, Union, Sequence, Optional, Dict, Set, Collection

from omg.database import TagAndValue, MusicDatabase
from omg.model.covers import CoverId, Cover
from omg.model.ids import OmgId
from omg.model.pieces import PieceOfMusic, PieceData
from omg.url import URL

log = logging.getLogger(__name__)


class TagCache:
    _tags_by_value: Dict[str, Dict[str, Set[OmgId]]]

    def __init__(self, pieces: Iterable[PieceOfMusic] = None):
        self._tags_by_value = {}
        if pieces is not None:
            for piece in pieces:
                self.register_piece(piece)

    def register_piece(self, piece: PieceOfMusic):
        for tag, values in piece.tags.items():
            for value in values:
                self.ensure_entry(tag, value, piece.id)

    def ensure_entry(self, tag, value, id: OmgId):
        tagset = self.get_tag_set(tag, value)
        tagset.add(id)

    def match_tags(self, query: str, tags: Collection[str] = None) -> List[TagAndValue]:
        result = []
        if tags is None:
            tags = []
        re_query = '(?i)' + re.escape(query)
        for value, tags_of_value in self._tags_by_value.items():
            if re.search(re_query, value) is not None:
                result.extend((tag, value) for tag in tags_of_value if tag in tags)
        return result

    def get_piece_ids(self, tag: str, value: str) -> Iterable[OmgId]:
        return self._tags_by_value.get(value, {}).get(tag, set())

    def get_tag_set(self, tag: str, value: str):
        value_dict = self.get_value_dict(value)
        if tag not in value_dict:
            value_dict[tag] = set()
        return value_dict[tag]

    def get_value_dict(self, value: str) -> Dict[str, Set[OmgId]]:
        if value not in self._tags_by_value:
            self._tags_by_value[value] = {}
        return self._tags_by_value[value]


class DictMusicDatabase(MusicDatabase):
    _piece_by_id: Dict[OmgId, PieceOfMusic]
    _cache: TagCache
    _covers: Dict[URL, Cover]

    def __init__(self, pieces: Dict[OmgId, PieceOfMusic] = None, covers: Dict[URL, Cover] = None):
        self._piece_by_id = pieces or {}
        self._cache = TagCache(self._piece_by_id.values())
        self._covers = covers or {}

    def get_piece(self, id: OmgId) -> PieceOfMusic:
        return self._piece_by_id[id]

    def lookup(self, url: URL) -> Optional[PieceOfMusic]:
        return [p for p in self._piece_by_id.values() if p.url == url][0]

    def get_pieces(self, ids: Iterable[OmgId] = None, data: Sequence[str] = None) -> Iterable[PieceData]:
        if ids is None:
            ids = self._piece_by_id.keys()
        return (self._piece_by_id[id].data() for id in ids)

    def has_piece(self, id: OmgId) -> bool:
        return id in self._piece_by_id

    def add_pieces(self, pieces: Sequence[PieceOfMusic]) -> None:
        for piece in pieces:
            self._piece_by_id[piece.id] = piece
            self._cache.register_piece(piece)

    def match_tags(self, search_term: str, tags: Collection[str] = None) -> List[TagAndValue]:
        return self._cache.match_tags(search_term, tags)

    def get_pieces_for_tags(self, tag: str, value: str) -> Iterable[PieceOfMusic]:
        return [self.get_piece(id) for id in self._cache.get_piece_ids(tag, value)]

    def delete_piece(self, id: Union[OmgId, URL]):
        if id is URL:
            elem = self.lookup(id)
            id = elem.id
        del self._piece_by_id[id]

    def update_piece(self, piece: PieceOfMusic):
        self._piece_by_id[piece.id] = piece

    def get_cover(self, url: URL) -> Cover:
        return self._covers[id]

    def get_cover_by_id(self, id: CoverId):
        return next(v for v in self._covers.values() if v.id == id)

    def add_cover(self, cover: Cover):
        self._covers[cover.url] = cover

    def get_covers(self) -> Sequence[Cover]:
        return list(self._covers.values())

    def update_cover(self, cover: Cover):
        assert cover.url in self._covers
        self._covers[cover.url] = cover

    def remove_cover(self, url: URL):
        del self._covers[url]

    def data(self):
        return self._piece_by_id, self._covers


class FileBasedDictMusicDatabase(DictMusicDatabase):
    path: Path

    def __init__(self, path: Path):
        clsname = FileBasedDictMusicDatabase.__name__
        pieces, covers = {}, {}
        if not path.exists():
            log.info(f'{path} not found - creating empty {clsname}')
        else:
            with path.open('rb') as p:
                pieces, covers = pickle.load(p)
                log.info(f'loading {clsname} from {path} with {len(pieces)} pieces and {len(covers)} covers')

        super().__init__(pieces, covers)

    def finalize(self):
        import pickle
        with self.path.open('wb') as p:
            pickle.dump(self.data(), p)
