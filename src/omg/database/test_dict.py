from pathlib import Path

import pytest
from uuid import uuid4

from omg.database.dict import DictMusicDatabase
from omg.model.covers import Cover
from omg.model.ids import OmgId
from omg.tests.testhelpers.pieces import mock_track


def test_dict_database_get_existing_track():
    track = mock_track()
    db = DictMusicDatabase({track.id: track})
    assert db.get_piece(track.id) == track


def test_dict_database_raise_not_existing_track():
    db = DictMusicDatabase()
    with pytest.raises(KeyError):
        db.get_piece(OmgId.omg(uuid4()))


def test_dict_database_all_covers():
    test_cover = Cover(Path('nix'), '1234')
    db = DictMusicDatabase(covers={test_cover.url: test_cover})
    assert db.get_covers() == [test_cover]
