from typing import Sequence, Mapping

TagValues = Sequence[str]
Tags = Mapping[str, TagValues]
Flags = Sequence[str]
