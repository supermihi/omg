from dataclasses import dataclass, field
from typing import Sequence, List

from omg.model import Tags, Flags
from omg.model.covers import CoverId
from omg.model.ids import OmgId
from omg.url import URL
from omg.util.tags import title, artist


@dataclass(frozen=True)
class PieceData:
    id: OmgId
    tags: Tags = None
    flags: Flags = None
    url: URL = None
    covers: Sequence[CoverId] = None
    children: Sequence[OmgId] = None
    is_album: bool = None
    album_type: str = None

    def is_complete(self):
        ans = self.tags is not None and self.flags is not None and self.url is not None and self.covers is not None and self.is_album is not None
        if not ans:
            return False
        if self.is_album:
            return self.album_type is not None and self.children is not None
        return True

    def title(self):
        return title(self.tags) if self.tags else '<no tags fetched>'

    def artist(self):
        return artist(self.tags) if self.tags else '<no tags fetched>'


@dataclass(frozen=True)
class PieceOfMusic:
    id: OmgId
    tags: Tags
    flags: Flags
    url: URL
    covers: Sequence[CoverId]

    def title(self):
        return title(self.tags)

    def artist(self):
        return artist(self.tags)

    def __str__(self):
        return f'{self.title()} by {self.artist()}'

    @staticmethod
    def from_data(data: PieceData):
        if not data.is_complete():
            raise ValueError(f'cannot create piece from incomplete data')
        if data.is_album:
            return Album(data.id, data.tags, data.flags, data.url, data.covers, data.children, data.album_type)
        return Track(data.id, data.tags, data.flags, data.url, data.covers)

    def data(self) -> PieceData:
        raise NotImplementedError()


@dataclass(frozen=True)
class Track(PieceOfMusic):
    """An individual audio track (playable file)."""

    def data(self) -> PieceData:
        return PieceData(self.id, self.tags, self.flags, self.url, self.covers, is_album=False)


@dataclass(frozen=True)
class Album(PieceOfMusic):
    """A container containing other albums or tracks."""

    children: Sequence[OmgId]
    type: str

    def data(self) -> PieceData:
        return PieceData(self.id, self.tags, self.flags, self.url, self.covers, self.children, True, self.type)


class BuiltinAlbumType:
    generic = 'generic'
    album = 'album'
    part = 'part'
    work = 'work'
    collection = 'collection'
    medium = 'medium'
    playlist = 'playlist'
