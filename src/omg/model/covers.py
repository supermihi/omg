from dataclasses import dataclass

from omg.url import URL

CoverId = str


@dataclass(frozen=True)
class Cover:
    url: URL
    id: CoverId
