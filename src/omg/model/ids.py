from dataclasses import dataclass
from enum import Enum
from os import fspath
from pathlib import Path
from uuid import UUID, uuid5, NAMESPACE_URL, uuid4

NAMESPACE_URI = 'http://gitlab.com/supermihi/omg'

OMG_NAMESPACE = uuid5(NAMESPACE_URL, NAMESPACE_URI)


class IdType(Enum):
    acoustid = 0
    musicbrainz = 1
    omg = 2


@dataclass(frozen=True)
class OmgId:
    id: UUID
    type: IdType

    @staticmethod
    def omg(id: UUID):
        return OmgId(id, IdType.omg)

    @staticmethod
    def musicbrainz(id: UUID):
        return OmgId(id, IdType.musicbrainz)

    @staticmethod
    def acoustid(id: UUID):
        return OmgId(id, IdType.acoustid)

    @staticmethod
    def random():
        return OmgId.omg(uuid4())

    def __str__(self):
        id_str = str(self.id)
        if self.type == IdType.omg:
            return id_str
        if self.type == IdType.acoustid:
            return f'acoustid:{id_str}'
        if self.type == IdType.musicbrainz:
            return f'mbid:{id_str}'
        raise ValueError(f'unknown id type: {self.type.name}')

    @staticmethod
    def from_string(id_str: str) -> 'OmgId':
        if id_str.startswith('acoustid:'):
            uuid = UUID(hex=id_str[len('acoustid:'):])
            return OmgId.acoustid(uuid)
        elif id_str.startswith('mbid:'):
            uuid = UUID(hex=id_str[len('mbid:'):])
            return OmgId.musicbrainz(uuid)
        return OmgId.omg(UUID(hex=id_str))


def create_omg_id(path: Path) -> OmgId:
    uuid = uuid5(OMG_NAMESPACE, fspath(path))
    return OmgId.omg(uuid)
