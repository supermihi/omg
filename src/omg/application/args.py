from argparse import ArgumentParser


def get_main_parser():
    parser = ArgumentParser(description='OMG')
    add_main_args(parser)
    return parser


def add_main_args(parser: ArgumentParser):
    parser.add_argument('r', '--r', metavar='ROOT', type=str, help='music collection root directory')
