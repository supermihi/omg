from pathlib import Path

from omg.url import URL


def test_from_path():
    path = Path('/a/b/c.mp3')
    url = URL.from_local_path(path)

    assert url.scheme == 'file'
    assert url.path == path
    assert url.extension == '.mp3'
    assert url.netloc == ''