from marshmallow import Schema
from pathlib import Path

from omg.util.schemas import PathField


class TestSchema(Schema):
    path = PathField()


def test_deserialize_path():
    obj = dict(path='/a/b/c')
    loaded = TestSchema().load(obj)
    assert loaded['path'] == Path('/a/b/c')


def test_serialize_path():
    object = dict(path=Path('/a/c/b'))
    dump = TestSchema().dump(object)
    assert dump['path'] == '/a/c/b'
