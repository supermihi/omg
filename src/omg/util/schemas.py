from pathlib import Path
from typing import List, Union

from marshmallow import fields

from omg.model.ids import OmgId


class PathField(fields.Field):
    """
    Marshmallow field for Paths.
    """

    def _serialize(self, value, attr, obj, **kwargs):
        return str(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return Path(value)


def serialize_tagvalues(values: List[str]):
    if len(values) == 1:
        return values[0]
    return values


def deserialize_tagvalues(values: Union[str, List[str]]):
    if isinstance(values, str):
        return [values]
    return values


class TagsField(fields.Field):
    """
    Marshmallow field for tags, which supports to set single-value tags as str instead of one-element lists.
    """

    def _serialize(self, value: dict, attr, obj, **kwargs):
        return {tag: serialize_tagvalues(values) for tag, values in value.items()}

    def _deserialize(self, value, attr, data, **kwargs):
        return {tag: deserialize_tagvalues(values) for tag, values in value.items()}


class OmgIdField(fields.Field):

    def _serialize(self, value: OmgId, attr: str, obj, **kwargs):
        return str(value)

    def _deserialize(self, value, attr, data, **kwargs):
        return OmgId.from_string(value)
