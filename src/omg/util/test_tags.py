from omg.util import tags as tested_module


def test_merge_tags_different_keys():
    tags_a = {'title': ['a title', 'b title']}
    tags_b = {'album': ['the best of']}

    expected = {'title': ['a title', 'b title'], 'album': ['the best of']}

    actual = tested_module.merge_tags(tags_a, tags_b)
    assert actual == expected


def test_merge_tags_same_key_same_values():
    tags_a = {'title': ['a title', 'b title'], 'album': ['the best of']}
    tags_b = {'title': ['a title', 'b title']}

    expected = {'title': ['a title', 'b title'], 'album': ['the best of']}

    actual = tested_module.merge_tags(tags_a, tags_b)
    assert actual == expected


def test_merge_tags_same_key_different_values():
    tags_a = {'title': ['a title']}
    tags_b = {'title': ['b title']}

    expected = {'title': ['a title', 'b title']}

    actual = tested_module.merge_tags(tags_a, tags_b)
    assert actual == expected


def test_merge_tags_same_key_partially_different_values():
    tags_a = {'title': ['a title']}
    tags_b = {'title': ['b title', 'a title']}

    expected = {'title': ['a title', 'b title']}

    actual = tested_module.merge_tags(tags_a, tags_b)
    assert actual == expected
