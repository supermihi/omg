from typing import Sequence, Dict, List, Union

from omg.model import Tags, TagValues


def get_tags_except(tags: Tags, excluded_tags: Sequence[str]) -> Tags:
    return {tag: values for tag, values in tags.items() if tag not in excluded_tags}


def merge_tags(*input_tags: Tags) -> Tags:
    result = {}
    for tags in input_tags:
        add_tags_to(result, tags)
    return result


def add_tags_to(tags_to_add_to: Dict[str, List[str]], tags_to_add: Tags):
    for tag, values in tags_to_add.items():
        add_tag_to(tags_to_add_to, tag, values)


def add_tag_to(tags_to_add_to: Dict[str, List[str]], tag: str, values: Union[TagValues, str]):
    if isinstance(values, str):
        values = [values]
    else:
        values = list(values)
    if tag not in tags_to_add_to:
        tags_to_add_to[tag] = values
    else:
        for value in values:
            if value not in tags_to_add_to[tag]:
                tags_to_add_to[tag].append(value)


def title(tags: Tags):
    return tags.get('title', ['<no title>'])[0]


def artist(tags):
    return tags.get('artist', ['<no artist>'])[0]
