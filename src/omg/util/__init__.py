from pathlib import Path

from typing import Sequence


def common_path(files: Sequence[Path]) -> Path:
    common_path = files[0].parent.parts
    for num_parts in range(len(common_path), 1, -1):
        if all(path.parts[:num_parts] == common_path[:num_parts] for path in files):
            return Path(*common_path[:num_parts])
