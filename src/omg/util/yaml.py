import yaml
from collections import OrderedDict

YAML_OPTIONS = dict(default_flow_style=False, allow_unicode=True, width=1000)


def setup_yaml():
    """ https://stackoverflow.com/a/8661021 """
    represent_dict_order = lambda self, data: self.represent_mapping('tag:yaml.org,2002:map', data.items())
    yaml.add_representer(OrderedDict, represent_dict_order)


setup_yaml()


def dump(obj, stream):
    return yaml.dump(obj, stream, **YAML_OPTIONS)


def load(content):
    return yaml.load(content, Loader=yaml.FullLoader)
