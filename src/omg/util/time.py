from datetime import datetime, timezone


def min_utc():
    return datetime.min.replace(tzinfo=timezone.utc)
