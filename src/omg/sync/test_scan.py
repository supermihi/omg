from datetime import datetime, timezone
from pathlib import Path

from fs.memoryfs import MemoryFS

from omg.config import default_config
from omg.sync.music_fs import DefaultMusicFS
from omg.sync.scan import get_filesystem_scan_result, EntityType, EntityScanData


def test_scanner_identifies_file_type():
    fs = MemoryFS()
    config = default_config(root='/')
    default_mtime = datetime(2019, 2, 9, 15, 2, tzinfo=timezone.utc)

    for file in ('/a.mp3', '/b.flac', '/c.png', '/d.jpg', '/e.album', '/f.album'):
        fs.touch(file)
        fs.settimes(file, default_mtime, default_mtime)

    music_fs = DefaultMusicFS(config, fs)
    result = get_filesystem_scan_result(music_fs)

    expected = {
        EntityScanData(Path('/e.album'), default_mtime, EntityType.album),
        EntityScanData(Path('/f.album'), default_mtime, EntityType.album),
        EntityScanData(Path('/c.png'), default_mtime, EntityType.cover),
        EntityScanData(Path('/d.jpg'), default_mtime, EntityType.cover),
        EntityScanData(Path('/a.mp3'), default_mtime, EntityType.track),
        EntityScanData(Path('/b.flac'), default_mtime, EntityType.track)
    }
    assert set(result) == expected


def test_scanner_logs_unknown_files():
    fs = MemoryFS()
    config = default_config(root='/')
    fs.touch('/unknown.odt')
    music_fs = DefaultMusicFS(config, fs)
    result = get_filesystem_scan_result(music_fs)
    other = [r for r in result if r.entity_type == EntityType.unknown]
    assert len(other) == 1
    assert other[0].path == Path('/unknown.odt')


def test_scanner_truncates_mtime_microseconds():
    fs = MemoryFS()
    config = default_config(root='/')
    fs.touch('/test.mp3')
    datetime_with_ms = datetime(2020, 1, 22, 14, 45, 12, 123)
    fs.setinfo('/test.mp3', {'details': {'modified': datetime_with_ms.timestamp()}})
    music_fs = DefaultMusicFS(config, fs)
    result = get_filesystem_scan_result(music_fs)[0]
    assert result.modified.microsecond == 0
