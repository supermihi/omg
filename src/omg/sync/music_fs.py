from abc import abstractmethod, ABCMeta
from pathlib import Path
from typing import Iterable

from fs.base import FS
from fs.osfs import OSFS

from omg import config
from omg.config import OmgConfig
from omg.files.tags_io import TagsIO, TaglibTagsIO


class MusicFS(metaclass=ABCMeta):
    @abstractmethod
    def is_album(self, path: Path):
        pass

    @abstractmethod
    def is_track(self, path: Path):
        pass

    @abstractmethod
    def is_image(self, path: Path):
        pass

    fs: FS
    music_root: Path
    tags_io: TagsIO


def has_extension(path: Path, extension: str):
    return path.suffix.lower() == f'.{extension}'


def has_any_extension(path: Path, extensions: Iterable[str]):
    return any(has_extension(path, e) for e in extensions)


class DefaultMusicFS(MusicFS):

    def __init__(self, config: OmgConfig, fs: FS = None):
        self.config = config
        self.fs = fs or OSFS('/')
        self.tags_io = TaglibTagsIO()

    def is_album(self, path: Path):
        return path.suffix.lower() == config.ALBUM_FILE_SUFFIX

    def is_track(self, path: Path):
        return has_any_extension(path, self.config.track_filetypes())

    def is_image(self, path: Path):
        return has_any_extension(path, self.config.image_filetypes())

    @property
    def music_root(self):
        return self.config.music_root()
