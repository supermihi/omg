from datetime import datetime
from pathlib import Path

from omg.files.constant_tags_io import ConstantTagsIO
from omg.sync.delta import delta_from_scan_results, DiffType
from omg.sync.scan import EntityScanData, EntityType


def test_delta_detects_new():
    disk_scan = [EntityScanData(Path('/old.mp3'), datetime(2019, 5, 12), EntityType.track),
                 EntityScanData(Path('/new.mp3'), datetime(2019, 6, 12), EntityType.track)]

    db_scan = [EntityScanData(Path('/old.mp3'), datetime(2019, 5, 13), EntityType.track)]

    diff = delta_from_scan_results(disk_scan, db_scan, ConstantTagsIO({'title': ['Demo']}))

    assert len(diff) == 1
    assert diff[0].diff_type == DiffType.only_on_disk
    assert diff[0].path == Path('/new.mp3')
