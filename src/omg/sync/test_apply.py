from datetime import datetime
from pathlib import Path

from omg.database.dict import DictMusicDatabase
from omg.sync.apply import apply_sync_result
from omg.sync.db import DictSyncDatabase
from omg.sync.delta import EntityDelta, DiffType
from omg.sync.scan import EntityType
from omg.tests.testhelpers.pieces import mock_track


def test_apply_calls_add():
    delta = EntityDelta(EntityType.track, DiffType.only_on_disk, Path('/none'), datetime(2020, 1, 7), mock_track())
    omg_db = DictMusicDatabase()
    sync_db = DictSyncDatabase()
    apply_sync_result([d for d in [delta]], omg_db, sync_db)

    assert len(list(omg_db.get_pieces())) == 1
    assert sync_db.last_check(delta.path) == delta.modified
