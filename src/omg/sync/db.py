import logging
import pickle
from abc import abstractmethod, ABC
from datetime import datetime
from pathlib import Path
from typing import Dict

from omg.config import OmgConfig

log = logging.getLogger(__name__)


class SyncDatabase(ABC):

    @abstractmethod
    def last_check(self, path: Path) -> datetime:
        raise NotImplementedError()

    @abstractmethod
    def set_last_check(self, path: Path, checked: datetime):
        raise NotImplementedError()

    @abstractmethod
    def delete_path(self, path: Path):
        raise NotImplementedError()

    @abstractmethod
    def finalize(self):
        pass


class DictSyncDatabase(SyncDatabase):
    data: Dict[Path, datetime]
    path: Path

    def __init__(self, initial: Dict[Path, datetime] = None, path: Path = None):
        self.data = initial or {}
        self.path = path

    def last_check(self, path: Path) -> datetime:
        return self.data.get(path, datetime.min)

    def set_last_check(self, path: Path, checked: datetime):
        self.data[path] = checked

    def delete_path(self, path: Path):
        del self.data[path]

    def finalize(self):
        with self.path.open('wb') as p:
            pickle.dump(self.data, p)

    @staticmethod
    def load_from_file(path: Path):
        import pickle
        if not path.exists():
            log.info(f'{path} not found - creating empty {DictSyncDatabase.__name__}')
            return DictSyncDatabase(path=path)
        with path.open('rb') as p:
            data = pickle.load(p)
            log.info(f'loaded {DictSyncDatabase.__name__} from {path} with {len(data)} entries')
            return DictSyncDatabase(data, path=path)


def create_default(config: OmgConfig):
    if config.db_type() == 'dict':
        return DictSyncDatabase.load_from_file(config.db_dir() / 'sync.db')
    else:

        from omg.database.peewee.peeweedb import PeeweeDatabase
        return PeeweeDatabase()
