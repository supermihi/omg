import logging
from dataclasses import dataclass
from datetime import datetime, timezone
from enum import Enum
from pathlib import Path

from typing import Sequence

from omg.database import MusicDatabase
from omg.model.pieces import Album
from omg.sync.db import SyncDatabase
from omg.sync.music_fs import MusicFS

log = logging.getLogger(__name__)


class EntityType(Enum):
    track = 0
    album = 1
    cover = 2
    unknown = 3


@dataclass(frozen=True)
class EntityScanData:
    path: Path
    modified: datetime
    entity_type: EntityType


ScanResult = Sequence[EntityScanData]


@dataclass
class FilesystemScanner:
    music_fs: MusicFS

    def scan(self) -> ScanResult:
        root = self.music_fs.music_root
        return list(self.scan_directory(root))

    def scan_directory(self, directory: Path):
        fs = self.music_fs.fs
        if not fs.exists(str(directory)):
            log.warning(f'not scanning directory because it does not exist: {directory}')
            return ()
        for path, info in fs.walk.info(str(directory), namespaces=['details']):
            if info.is_dir:
                continue
            yield self.scan_file(Path(path), info.modified.astimezone(timezone.utc).replace(microsecond=0))

    def scan_file(self, path: Path, mtime: datetime):
        type = get_entity_type(path, self.music_fs)
        return EntityScanData(path, mtime, type)


def get_filesystem_scan_result(music_fs: MusicFS):
    return FilesystemScanner(music_fs).scan()


def get_entity_type(path: Path, music_fs: MusicFS) -> EntityType:
    if music_fs.is_album(path):
        return EntityType.album
    if music_fs.is_track(path):
        return EntityType.track
    if music_fs.is_image(path):
        return EntityType.cover
    return EntityType.unknown


def get_db_scan_result(db: MusicDatabase, sync: SyncDatabase) -> ScanResult:
    pieces = db.get_pieces()

    for piece in pieces:
        mtime = sync.last_check(piece.url.path)
        type = EntityType.album if isinstance(piece, Album) else EntityType.track
        yield EntityScanData(piece.url.path, mtime, type)

    for cover in db.get_covers():
        yield EntityScanData(cover.url.path, sync.last_check(cover.url.path), EntityType.cover)
