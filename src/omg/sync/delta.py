import logging
from dataclasses import dataclass
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import Sequence, Dict, List, Optional, Union

from omg.database import MusicDatabase
from omg.files.album_io import read_album_data
from omg.files.cover import read_cover_data
from omg.files.tags import read_track_data
from omg.files.tags_io import TagsIO
from omg.model.covers import Cover
from omg.model.pieces import PieceOfMusic
from omg.sync.db import SyncDatabase
from omg.sync.music_fs import MusicFS
from omg.sync.scan import ScanResult, EntityType, EntityScanData, get_filesystem_scan_result, get_db_scan_result
from omg.url import URL

log = logging.getLogger(__name__)


class DiffType(Enum):
    only_on_disk = 0
    only_in_db = 1
    modified = 2


@dataclass
class EntityDelta:
    entity_type: EntityType
    diff_type: DiffType
    path: Path
    modified: datetime = None
    entity: Optional[Union[PieceOfMusic, Cover]] = None

    def __str__(self):
        return f'{self.diff_type.name} {self.entity_type.name} {self.path}, modified {self.modified}'

    def __repr__(self):
        return str(self)


SyncDelta = Sequence[EntityDelta]


def scan_filesystem_vs_db_delta(omg_db: MusicDatabase, sync_db: SyncDatabase, music_fs: MusicFS) -> SyncDelta:
    fs_scan_result = get_filesystem_scan_result(music_fs)
    db_scan_result = get_db_scan_result(omg_db, sync_db)
    return delta_from_scan_results(fs_scan_result, db_scan_result, music_fs.tags_io)


def delta_from_scan_results(on_disk: ScanResult, in_db: ScanResult, tags_io: TagsIO) -> SyncDelta:
    db_by_path: Dict[Path, EntityScanData] = {pwm.path: pwm for pwm in in_db}
    result: List[EntityDelta] = []
    for entity in on_disk:
        if entity.entity_type == EntityType.unknown:
            log.info(f'skipping unknown file {entity.path}')
            continue
        in_db = db_by_path.get(entity.path, None)
        if in_db is None:
            ent = load_entity(entity.path, entity.entity_type, tags_io)
            result.append(EntityDelta(entity.entity_type, DiffType.only_on_disk, entity.path, entity.modified, ent))
            continue
        if in_db.modified < entity.modified:
            ent = load_entity(entity.path, entity.entity_type, tags_io)
            result.append(EntityDelta(entity.entity_type, DiffType.modified, entity.path, entity.modified, ent))
        del db_by_path[entity.path]
    deleted = [EntityDelta(entity.entity_type, DiffType.only_in_db, entity.path) for entity in db_by_path.values()]
    result.extend(deleted)
    return result


def load_entity(path: Path, type: EntityType, tags_io: TagsIO):
    url = URL.from_local_path(path)
    if type == EntityType.track:
        return read_track_data(path, tags_io).to_track(url)
    elif type == EntityType.album:
        return read_album_data(path).to_album(url)
    elif type == EntityType.cover:
        return read_cover_data(path).to_cover(url)
    else:
        raise KeyError(f'unknown entity type: {type}')
