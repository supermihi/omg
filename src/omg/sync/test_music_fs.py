from pathlib import Path

from omg.sync.music_fs import has_extension


def test_has_extension():
    assert has_extension(Path('/x.flac'), 'flac')
    assert not has_extension(Path('/usr/bin'), 'mp3')
    assert has_extension(Path('/path/to/my.MP3'), 'mp3')
    assert not has_extension(Path('/dir/named/mp3'), 'mp3')
