from itertools import groupby
from typing import Iterable
import logging

from omg.database import MusicDatabase
from omg.sync.delta import SyncDelta, DiffType, EntityDelta
from omg.sync.db import SyncDatabase
from omg.sync.scan import EntityType
from omg.url import URL

log = logging.getLogger(__name__)


def apply_sync_result(result: SyncDelta, db: MusicDatabase, sync_db: SyncDatabase):
    by_type = {type: list(diffs_of_type) for type, diffs_of_type in groupby(result, lambda diff: diff.diff_type)}
    update_modified_entities(by_type.get(DiffType.modified, []), db, sync_db)
    remove_entities_no_longer_on_disk(by_type.get(DiffType.only_in_db, []), db, sync_db)
    add_new_entities(by_type.get(DiffType.only_on_disk, []), db, sync_db)


def update_modified_entities(entities: Iterable[EntityDelta], omg_db: MusicDatabase, sync_db: SyncDatabase):
    for entity in entities:
        update_modified_entity(entity, omg_db, sync_db)


def update_modified_entity(entity, omg_db: MusicDatabase, sync_db):
    log.debug(f'updating {entity} in database')
    if entity.entity_type == EntityType.cover:
        omg_db.update_cover(entity.entity)
    else:
        omg_db.update_piece(entity.entity)
    sync_db.set_last_check(entity.path, entity.modified)


def add_new_entity(entity: EntityDelta, omg_db: MusicDatabase, sync_db):
    log.debug(f'adding {entity.entity_type.name} {entity.path} to database')
    if entity.entity_type == EntityType.cover:
        omg_db.add_cover(entity.entity)
    else:
        omg_db.add_pieces([entity.entity])
    sync_db.set_last_check(entity.path, entity.modified)


def add_new_entities(entities: Iterable[EntityDelta], omg_db: MusicDatabase, sync_db: SyncDatabase):
    for entity in entities:
        add_new_entity(entity, omg_db, sync_db)


def remove_entity_no_longer_on_disk(delta: EntityDelta, omg_db: MusicDatabase, sync_db: SyncDatabase):
    log.debug(f'removing {delta.entity_type.name} {delta.path} from database')
    if delta.entity_type == EntityType.cover:
        omg_db.remove_cover(URL.from_local_path(delta.path))
    else:
        omg_db.delete_piece(URL.from_local_path(delta.path))
    sync_db.delete_path(delta.path)


def remove_entities_no_longer_on_disk(entities: Iterable[EntityDelta], omg_db: MusicDatabase, sync_db: SyncDatabase):
    for entity in entities:
        remove_entity_no_longer_on_disk(entity, omg_db, sync_db)
