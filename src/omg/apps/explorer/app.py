import logging
import sys

from playhouse.test_utils import count_queries

from omg import database
from omg.config import OmgConfig


def app():
    logging.basicConfig(level=logging.DEBUG, format='%(asctime)-15s %(levelname)s:%(name)s %(message)s')
    config = OmgConfig()
    omg_db = database.create_default(config, db_loglevel=logging.INFO)
    search_tags = {'artist',  'title', 'composer', 'performer '}
    queries = sys.argv[1:]
    matching_tags = [omg_db.match_tags(query, search_tags) for query in queries]
    print(f'found matched tags. now, collecting pieces ...')
    with count_queries() as counter:
        pieces = omg_db.get_pieces_for_tags(matching_tags, True, 'tags')
    print(f'{counter.count} queries')
    for piece in pieces:
        print(
            f'  matching {"album" if piece.is_album else "track"}: {piece.title()} by {piece.artist()}')


if __name__ == '__main__':
    app()
