import logging

from omg import database
from omg.config import OmgConfig
from omg.sync import db as sync_database
from omg.sync.apply import apply_sync_result
from omg.sync.delta import scan_filesystem_vs_db_delta
from omg.sync.music_fs import DefaultMusicFS


def app():
    logging.basicConfig(level=logging.DEBUG)
    config = OmgConfig()
    omg_db = database.create_default(config)
    sync_db = sync_database.create_default(config)

    delta = scan_filesystem_vs_db_delta(omg_db, sync_db, DefaultMusicFS(config))
    apply_sync_result(delta, omg_db, sync_db)

    omg_db.finalize()
    sync_db.finalize()


if __name__ == '__main__':
    app()
