from dataclasses import dataclass
from maestro.core.elements import Element
from pathlib import Path
from typing import Optional

from omg.files.album import CoverFile
import omg.files.cover


@dataclass
class MaestroCoverData:
    maestro_path: Path
    cover_info: CoverFile


class NoCoverException(Exception):
    pass


def get_maestro_cover_path(element: Element):
    cover_path = element.getCoverPath()
    if cover_path is None:
        raise NoCoverException()
    return Path(cover_path)


def get_cover_data(maestro_cover_path: Path, album_path: Path) -> Optional[MaestroCoverData]:
    if maestro_cover_path is None:
        return None
    blake = omg.files.cover.get_blake(maestro_cover_path)
    omg_cover_path = omg.files.cover.get_proposed_cover_path(album_path, maestro_cover_path)
    cover_info = CoverFile(omg_cover_path.relative_to(album_path.parent), blake)
    return MaestroCoverData(maestro_path=maestro_cover_path, cover_info=cover_info)
