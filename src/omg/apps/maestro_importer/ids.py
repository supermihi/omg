import uuid


class MaestroIdToOmgIdConverter:

    def __init__(self, namespace: str = 'maestro'):
        maestro_id = uuid.uuid5(uuid.NAMESPACE_URL, 'https://pypi.python.org/pypi/maestro')
        self._namespace_id = uuid.uuid5(maestro_id, namespace)

    def convert_to_omg_id(self, maestro_id: int) -> uuid.UUID:
        return uuid.uuid5(self._namespace_id, str(maestro_id))

