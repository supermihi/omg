import logging
from maestro.core.tags import TagDict, TagValueList, Tag

from omg.model import Tags

logger = logging.getLogger(__name__)


def to_omg_tags(maestro_tags: TagDict) -> Tags:
    result = {}
    for maestro_tag, maestro_values in maestro_tags.items():
        if maestro_tag.private:
            logger.warning(f'private tag {maestro_tag.name.lower()}')
        str_key, str_values = convert_to_string_key_and_values(maestro_tag, maestro_values)
        result[str_key] = str_values
    return result


def convert_to_string_key_and_values(maestro_tag: Tag, maestro_values: TagValueList):
    key = maestro_tag.name.lower()
    values = [maestro_tag.type.fileFormat(value) for value in maestro_values]
    return key, values
