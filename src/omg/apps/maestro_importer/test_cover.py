from pathlib import Path

import pytest

import omg.files.cover


@pytest.mark.skip()
def test_get_cover_data(monkeypatch):
    from omg.apps.maestro_importer.covers import get_cover_data
    maestro_path = Path('/path/to/maestro/cover.jpg')
    album_path = Path('/path/to/omg/music/some album.album')
    monkeypatch.setattr(omg.files.cover, 'get_blake', lambda p: '12345')
    cover_data = get_cover_data(maestro_path, album_path)

    assert cover_data.maestro_path == maestro_path
    assert cover_data.cover_info.blake == '12345'
    assert cover_data.cover_info.path == Path('some album.jpg')
