import shutil
from datetime import datetime
from maestro.core.elements import File
from os import PathLike
from pathlib import Path

from omg.files.tags import ID_TAG, supports_id_tag
from omg.util.fs import Filesystem


class MaestroToOmgPathConverter:

    def __init__(self, maestro_base: PathLike, omg_base: PathLike, fs: Filesystem):
        self.maestro_base = Path(maestro_base).absolute()
        self.omg_base = Path(omg_base).absolute()
        self.fs = fs

    def get_target_path(self, maestro_file_path: Path) -> Path:
        assert self.maestro_base in maestro_file_path.parents
        relative = maestro_file_path.relative_to(self.maestro_base)
        return self.omg_base / relative

    def is_processed(self, maestro_file_path: Path):
        if self.is_inplace():
            return self.is_processed_inplace(maestro_file_path)
        return self.get_target_path(maestro_file_path).exists()

    def is_processed_inplace(self, path: Path):
        supports_id = supports_id_tag(path)
        if not supports_id:
            mtime = self.fs.modification_time(path)
            is_new = mtime > datetime(2019, 1, 1)
            if is_new:
                return True
        if ID_TAG in read_file_tags(path):
            return True
        return False

    def ensure_target(self, maestro_file_path: Path) -> Path:
        if self.is_inplace():
            return maestro_file_path

        target = self.get_target_path(maestro_file_path)
        if not target.exists():
            copy_file(maestro_file_path, target)
        return target

    def is_inplace(self):
        return self.maestro_base == self.omg_base


def get_maestro_file_path(file: File):
    return Path(file.url.path)


def copy_file(source: Path, destination: Path):
    destination.parent.mkdir(parents=True, exist_ok=True)
    shutil.copyfile(source, destination)
