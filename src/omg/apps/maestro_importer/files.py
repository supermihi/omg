import logging
from maestro.core.elements import File as MaestroFile
import maestro.database as db
from maestro.filesystem import RealFile
from pathlib import Path

from omg.apps.maestro_importer.conflicts import TagConflictResolver, MaestroVersusFileTagConflict, ConflictResolution
from omg.apps.maestro_importer.flags import to_omg_flags
from omg.apps.maestro_importer.paths import MaestroToOmgPathConverter, get_maestro_file_path
from omg.apps.maestro_importer.ids import MaestroIdToOmgIdConverter
from omg.apps.maestro_importer.tags import to_omg_tags
from omg.files.tags import save_in_track_data, read_file_tags, get_tags_except
from model.pieces import TrackData

from omg.files.tags_io import TaglibTagsIO
from omg.model import Tags
from omg.util.tags import merge_tags

logger = logging.getLogger(__name__)


class MaestroFileExporter:
    path_converter: MaestroToOmgPathConverter
    id_converter: MaestroIdToOmgIdConverter
    tag_conflict_resolver: TagConflictResolver

    def __init__(self, id_converter: MaestroIdToOmgIdConverter, path_converter: MaestroToOmgPathConverter,
                 tag_conflict_resolver: TagConflictResolver):
        self.id_converter = id_converter
        self.path_converter = path_converter
        self.tag_conflict_resolver = tag_conflict_resolver
        self.tagsio = TaglibTagsIO()

    def export_maestro_file(self, file: MaestroFile):
        maestro_path = get_maestro_file_path(file)
        if self.path_converter.is_processed(maestro_path):
            logger.info(f'skipping already processed file {maestro_path.relative_to(self.path_converter.maestro_base)}')
            return
        target = self.path_converter.ensure_target(maestro_path)
        self.export_to_tags(file, target)

    def export_to_tags(self, file: MaestroFile, target_path: Path):
        """Exports maestro File data to the tags of the on-disk file."""
        data = self.create_in_track_data(file)
        logger.info(f'Creating file {target_path}...')
        save_in_track_data(target_path, data, self.tagsio)

    def create_in_track_data(self, file: MaestroFile) -> TrackData:
        self.check_can_convert(file)
        tags = self.merge_file_and_maestro_db_tags(file)
        id = self.id_converter.convert_to_omg_id(file.id)
        flags = to_omg_flags(file.flags)
        in_track_data = TrackData(id, flags, tags)
        return in_track_data

    def get_target_path(self, file: MaestroFile) -> Path:
        maestro_path = get_maestro_file_path(file)
        target_path = self.path_converter.get_target_path(maestro_path)
        return target_path

    def check_can_convert(self, file: MaestroFile):
        if file.hasCover():
            raise NotImplementedError(f'export of file covers not implemented')

    def merge_file_and_maestro_db_tags(self, file: MaestroFile):
        file_tags = read_file_tags(get_maestro_file_path(file))
        new_file_tags = self.get_merged_maestro_public_and_file_tags(file, file_tags)
        private_tags = to_omg_tags(file.tags.privateTags())
        fingerprint_tags = extract_fingerprint_tag(file)
        all_tags = merge_tags(new_file_tags, private_tags, fingerprint_tags)
        return all_tags

    def get_merged_maestro_public_and_file_tags(self, file: MaestroFile, file_tags: Tags):
        public_maestro_db_tags = to_omg_tags(file.tags.withoutPrivateTags())

        not_excluded_file_tags = get_tags_except(file_tags, RealFile.specialTagNames)
        if public_maestro_db_tags == not_excluded_file_tags:
            return file_tags
        else:
            conflict = MaestroVersusFileTagConflict(public_maestro_db_tags, not_excluded_file_tags,
                                                    get_maestro_file_path(file))
            resolution = self.tag_conflict_resolver.resolve(conflict)
            if resolution == ConflictResolution.file:
                return file_tags
            special_tags = {t: v for t, v in file_tags.items() if t in RealFile.specialTagNames}
            return merge_tags(public_maestro_db_tags, special_tags)


def extract_fingerprint_tag(file: MaestroFile):
    maestro_hash: str = db.query('SELECT hash FROM files WHERE element_id=?', file.id).getSingle()
    if maestro_hash.startswith('mbid'):
        musicbrainz_id = maestro_hash[len('mbid:'):]
        return {'musicbrainz_trackid': [musicbrainz_id]}
    elif maestro_hash.startswith('acoustid'):
        acoustid_id = maestro_hash[len('acoustid:'):]
        return {'acoustid_id': [acoustid_id]}
    logger.warning(f'no fingerprint for {file.url}')
    return {}


class ConsistencyError(Exception):
    pass
