import argparse
import logging
import sys
from typing import List, Set

import maestro.database as db
from fs.osfs import OSFS
from maestro import application as maestro_application
from maestro.core import levels, elements as maestro_elements
from maestro import config as maestro_config
import maestro.logging

from omg.apps.maestro_importer.conflicts import ConsoleTagConflictResolver
from omg.apps.maestro_importer.containers import MaestroContainerExporter
from omg.apps.maestro_importer.files import MaestroFileExporter
from omg.apps.maestro_importer.ids import MaestroIdToOmgIdConverter
from omg.apps.maestro_importer.paths import MaestroToOmgPathConverter

maestro.logging._init = lambda: None

logger = logging.getLogger(__name__)


def load_with_children(elems: List[maestro_elements.Element], loaded: Set[maestro_elements.Element] = None):
    if loaded is None:
        loaded = set()
    ret = []
    for elem in elems:
        if isinstance(elem, maestro_elements.Container):
            children = levels.real.collect(elem.contents.ids)
            ret += load_with_children(children, loaded)
            loaded.update(c.id for c in children)
        if elem.id not in loaded:
            ret.append(elem)
    return ret


class MaestroImporter:

    def __init__(self, args):
        self.args = args

    def run(self):
        del sys.argv[1:]  # maestro parses args by itself ...
        maestro_application.run(type='noplugins', exitPoint='noplugins')
        logging.basicConfig(level=logging.DEBUG)
        logger.info('maestro started in database mode')
        limit = f'LIMIT {self.args.max}' if self.args.max else ''
        ids = list(db.query(f'SELECT id FROM elements {limit}').getSingleColumn())
        ids = [id for id in ids if id not in range(25134, 25175)]
        logger.debug(f'maestro has {len(ids)} elements')
        maestro_elems = load_with_children(levels.real.collect(ids))
        logger.info(f'collected {len(maestro_elems)} maestro elements')
        id_converter = MaestroIdToOmgIdConverter('omg')
        path_converter = MaestroToOmgPathConverter(maestro_config.storage.filesystem.sources[0]['path'], self.args.path,
                                                   OSFS('/'))
        container_exporter = MaestroContainerExporter(id_converter, path_converter)
        file_exporter = MaestroFileExporter(id_converter, path_converter, ConsoleTagConflictResolver())

        for elem in maestro_elems:
            if isinstance(elem, maestro_elements.Container):
                container_exporter.export_album(elem)
                # file_exporter.export_maestro_file(elem)


def run():
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(description='omg command-line interface')
    parser.add_argument('--max', '-m', type=int, help='max number of elements to convert')
    parser.add_argument('path', help='target path for music')
    args = parser.parse_args()
    importer = MaestroImporter(args)
    importer.run()


if __name__ == '__main__':
    run()
