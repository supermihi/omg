from abc import abstractmethod
from dataclasses import dataclass
from enum import Enum
from pathlib import Path
from pprint import pprint

from omg.model import Tags


@dataclass
class MaestroVersusFileTagConflict:
    maestro_tags: Tags
    file_tags: Tags
    path: Path


class ConflictResolution(Enum):
    maestro = 0,
    file = 1


class TagConflictResolver:

    @abstractmethod
    def resolve(self, conflict: MaestroVersusFileTagConflict) -> ConflictResolution:
        pass


class ConsoleTagConflictResolver(TagConflictResolver):

    def resolve(self, conflict: MaestroVersusFileTagConflict) -> ConflictResolution:
        print(f'tag conflict in {conflict.path}')
        only_maestro, only_file = only_different_tags(conflict.maestro_tags, conflict.file_tags)
        if only_order_differs(only_maestro, only_file):
            return ConflictResolution.file
        print(f'maestro db tags:')
        pprint(only_maestro, indent=2)
        print(f'file tags:')
        pprint(only_file, indent=2)
        choice = input('[m]aestro or [f]ile? ')
        if choice == 'm':
            return ConflictResolution.maestro
        elif choice == 'f':
            return ConflictResolution.file
        print('unknown answer. try again')
        return self.resolve(conflict)


def only_different_tags(one: Tags, two: Tags):
    same = [k for k in set(one.keys()).intersection(two.keys()) if one[k] == two[k]]
    only_one = {k: v for k, v in one.items() if k not in same}
    only_two = {k: v for k, v in two.items() if k not in same}
    return only_one, only_two


def only_order_differs(a: Tags, b: Tags):
    if set(a.keys()) != set(b.keys()):
        return False
    for key in a.keys():
        if set(a[key]) != set(b[key]):
            return False
    return True
