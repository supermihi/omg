import logging
import shutil
from maestro.core.elements import Container, ContainerType, Element
from pathlib import Path
from typing import Tuple, Sequence

from omg.apps.maestro_importer.covers import get_cover_data, get_maestro_cover_path, MaestroCoverData, NoCoverException
from omg.apps.maestro_importer.flags import to_omg_flags
from omg.apps.maestro_importer.ids import MaestroIdToOmgIdConverter
from omg.apps.maestro_importer.paths import MaestroToOmgPathConverter, get_maestro_file_path
from omg.apps.maestro_importer.tags import to_omg_tags
from omg.files.album import AlbumData, ChildRef
from omg.model import Tags
from omg.model.pieces import BuiltinAlbumType

logger = logging.getLogger(__name__)


class MaestroContainerExporter:

    def __init__(self, id_converter: MaestroIdToOmgIdConverter, path_converter: MaestroToOmgPathConverter):
        self.id_converter = id_converter
        self.path_converter = path_converter

    # noinspection PyTypeChecker
    def get_omg_path(self, el: Element):
        if el.isFile():
            return get_maestro_file_path(el)
        path = self.export_album(el)
        return path

    def get_children_paths(self, container: Container):
        maestro_children = container.getContents()
        omg_children_paths = [self.get_omg_path(e) for e in maestro_children]
        return omg_children_paths

    def export_album(self, container: Container) -> Path:
        id = self.id_converter.convert_to_omg_id(container.id)
        albumtype = get_album_type_for_container_type(container.type)
        tags = to_omg_tags(container.tags)
        flags = to_omg_flags(container.flags)

        omg_children_paths = self.get_children_paths(container)
        album_path = self.get_album_path(tags, omg_children_paths)
        if album_path.exists():
            logging.info(f'skipping existing album {album_path}')
            return album_path

        omg_child_infos = [self.create_child_info(album_path, p, id) for p, id in
                           zip(omg_children_paths, container.contents.ids)]

        covers = self.get_covers(container, album_path)
        album = AlbumData(id, albumtype, tags, flags, [cover.cover_info for cover in covers], omg_child_infos)
        for cover in covers:
            self.copy_cover(cover, album_path.parent)
        self.write_album(album, album_path)
        return album_path

    def get_covers(self, container: Container, album_path: Path):
        try:
            maestro_cover_path = get_maestro_cover_path(container)
            cover = get_cover_data(maestro_cover_path, album_path)
            return [cover]
        except NoCoverException:
            return []

    def write_album(self, album: AlbumData, path: Path):
        album.to_file(path)

    def copy_cover(self, cover: MaestroCoverData, target_dir: Path):
        shutil.copy(cover.maestro_path, target_dir / cover.cover_info.path)

    def get_album_path(self, tags: Tags, children_paths: Sequence[Path]):

        album_path = AlbumData.propose_path(tags, children_paths)
        return album_path

    def create_child_info(self, album_path: Path, child_path: Path, maestro_id):
        omg_id = self.id_converter.convert_to_omg_id(maestro_id)
        relative_child_path = child_path.relative_to(album_path.parent)
        return ChildRef(relative_child_path, omg_id)


def get_album_type_for_container_type(container_type: ContainerType) -> BuiltinAlbumType:
    if container_type == ContainerType.Album:
        return BuiltinAlbumType.album
    elif container_type == ContainerType.Work:
        return BuiltinAlbumType.work
    elif container_type == ContainerType.Collection:
        return BuiltinAlbumType.collection
    else:
        return BuiltinAlbumType.generic
