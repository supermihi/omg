from maestro.core.flags import Flag
from typing import Sequence

from omg.model import Flags


def to_omg_flags(maestro_flags: Sequence[Flag]) -> Flags:
    return [str(flag) for flag in maestro_flags]

