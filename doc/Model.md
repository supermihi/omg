## Data Model

### Element Hierarchy

In OMG, your music is organized as a directed acyclic graph (DAG) of
[*elements*](concepts/Element.md). Each leaf (vertex without descendants)
in this graph corresponds to a  [*track (audio file)*](concepts/Track.md). All other
vertices are called [*albums*](concepts/Album.md). They are represented by special
text files in the music collection directory.

The immediate descendants of an album are called its *children*. While we use
the term *album* for convenience, an album in OMG is not only used to represent
an album in the common meaning, but instead is a general collection of tracks
(and potentially nested "albums"), for example:
- the recording of a classical work, with the children being the indivdual movements,
- a collection box of several albums of an artist, each child of which is again a
  container (one album in the collection), whose children (the grand-children of the former)
  finally correspond to the recordings of that album.
  
The hierarchies may become arbitrarily complex:
- an element can be the child of more than one container. For example, a studio album
  from *The Beatles* might be contained in several collection boxes.
- a container can have both files and other containers as its direct descendants.
  For example, a classical album may contain both individual pieces and a symphony
  which in turn consists of three movements.
  

### Concepts
- [tags](concepts/Tags.md)
- [flags](concepts/Flags.md)
