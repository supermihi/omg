# OMG documentation

## Model
The [model](Model.md) documentation explains how your music collection is modelled in OMG.

## Exploring
OMG's main task is to help you explore your music collection and enable you to quickly
find the music that you want to listen to next. There are multiple means of exploration:
- [searching](Search.md),
- browsing by tag keys (e.g. by artist, by composer)
- browsing by file system structure.
