## Search

The most efficient way to find elements in your database is via a textual search. This
document explains how a search is evaluated.

One usually starts off with a *search string* that consists of one or more *search tokens*
separated by spaces, e.g.:
```
beatles pepper
```
The actual search consists of several phases:
- [tag matching](#tag-matching)
- [element matching](#element-matching)
- [tree reduction](#tree-reduction)

### Tag Matching
For each search token, OMG determines the list of *key=value* tags the value of which
contains the token. This search is case- and diacritics-insensitive. In the above example,
this might lead to the following result:
```
beatles:
  - artist="The Beatles"
  
pepper:
  - artist="Red Hot Chili Peppers"
  - title="Sgt. Pepper's Lonely Hearts Club Band"
  - album="Sgt. Pepper's Lonely Hearts Club Band"
```

For improved search experience, the result of tag matching might be directly fed back
to the user, allowing to select the intented tag.

### Element Matching
Next, OMG determines the list of elements that match *at least one* tag from *each token's
match list*. Continuing the example, we search for an element with `artist=The Beatles`
AND either
- artist="Red Hot Chili Peppers"
- title="Sgt. Pepper's Lonely Hearts Club Band"
- album="Sgt. Pepper's Lonely Hearts Club Band"

This might return the following elements:
```
- Sgt. Pepper's Lonely Hearts Club Band (container with 13 Elements)
- Sgt. Pepper's Lonely Hearts Club Band.flac (file contained in the above container)
- Sgt. Pepper's Lonely Hearts Club Band.flac (file contained in "Blue Album")
```


### Tree Reduction

The return of element matching is a flat list of elements, containing both containers
and files. Moreover, as in the above example, it can easily contain both a container
and some of its children (or grandchildren).

In order to support a concise presentation of the search results, OMG removes all
elements from the search result that are descendants of any of the remaining elements.