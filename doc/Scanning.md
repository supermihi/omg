# Scanning
This document describes scanning and synchronization of disc content with OMG's internal database.

At startup, OMG performs the steps
- load database
- scan filesystem
- record changes
- resolve changes

## Load Database
Depending on database type, loads a file or connects to a database server.

## Scan Filesystem
Scans the collection directory, collecting all relevant album, track and cover files together with their modification timestamp.

## Record Changes
Compares the database with the scanned filesystem. The following may occur:

### New Files
A new file (album, track or cover) is found on disc that is not yet in the database. In that case, read its contents
(tags, flags, id etc. for pieces, hash for covers). Tracks are assigned a new id by default and their hash is computed.

### Modified Files
A known file (track, album or cover) has been modified since the last synchronization. In that case, read its content,
much like in the *new file* case.

### Deleted Files
A known file (track, album, or cover) is missing on disk. Record its path.

## Resolve Changes

