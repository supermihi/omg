### Tags

Besides element hierarchy, *tags* are the most important concept
in OMG that allows you to categorize your music.

Any element (be it container or file) can have an arbitrary number of *tags*. Tags are
textual metadata organized as a list of *key=value* pairs describing an element, for example:
- artist: The Beatles
- date: 1968-11-22
- title: White Album
- Genre: Rock

The same key might appear more than once (for instance, a work can have several composers).

Tags of files are stored within the files, whereas container tags reside in OMG's database
only.