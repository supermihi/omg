## Piece

A *piece of music* is the central item in OMG's music collection model. Each piece has:
 - a unique id (UUID),
 - [tags](Tags.md),
 - [flags](FLags.md).
 
 It is either an [album](Album.md) or a [track](Track.md).