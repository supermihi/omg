## Album

An album is a [piece](Piece.md) that has children. Albums are represented by
YAML files that describe their tags, children, and some other properties.

An album has an ordered list of *children*, each of which can be either another
album or a [track](Track.md).

### YAML file syntax
TODO