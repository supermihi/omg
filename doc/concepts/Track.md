## Tracks

A track is a special [piece](Piece.md) that corresponds to a real audio file in your
music collection.