### Flags

In addition to its tags, each element can have a list of *flags*. Flags are binary information
(a flag is either present or not) and are used to further categorize music. Example flags
are:
- *favourite*
- *downloaded from the internet*
- *party music*

Flags are always stored in the database only.