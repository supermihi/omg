# Data

## Pieces of Music: Data Model

### General

The common properties of all pieces are:

|name  |type                  |description|
|------|----------------------|-----------|
|`id`  |UUID  string          |unique id   |
|`tags`|map of strings (tags) to lists of strings (values) |element's tags|
|`flags`|list of strings      |element's flags      |
|`url` |URL string | of the audio file or container yaml file |
|`length`|timespan|length of the element; for containers, the total length of its children ||

### Files

Additonal file properties:

|name  |type                  |description|
|------|----------------------|-----------|
|`fingerprint`|string         |acoustid fingerprint|

### Albums

Additional container properties:

|name  |type                  |description|
|------|----------------------|-----------|
|`children`|list of UUID strings|childrens' ids|

## On Disc
### Files
All tags of files are stored in the file itself. Flags are stored in comma-separated form in the tag `flags`.

The UUID of a file is stored in its `omg_id` tag.

**Note**: At the current stage, in-file flags and UUIDs are not implemented.

### Containers
The data of a container is stored in a `yml` file which by convenience should be located in the most specific common directory of all its children.

**Note**: Currently, persistent containers are not implemented.