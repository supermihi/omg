# Introduction

For fast lookup and searching, OMG stores all element's data in a MongoDB database. The documents in this folder describe the collections' layouts and purpose.

## Elements

The `elements` collection is the main collection which basically contains the metadata read from disk. It contains one document for each element, with the following structure:

### File
```json
{
  "_id": UUID("cf14170b-a865-5350-a2a2-a435c9da6841"),
  "core": {
}
```
### General

|name  |type                  |description|
|------|----------------------|-----------|
|`_id` |UUID                  |uniqe id   |
|`tags`|`Dict[str, List[str]` |tags mapping tag name to list of values|
|`flags`|`List[str]`          |flags      |
|`type` |`file` or `container`||
|`verified`|datetime | last time the on-disc file was synchronized|

### Files

|name  |type                  |description|
|------|----------------------|-----------|
|`url` |string                |url of the file |
|`fingerprint`|string         |acoustid fingerprint|
|`children`|`List[str]`|children's UUIDs|