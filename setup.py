from setuptools import setup, find_packages

setup(
    name='omg',
    version='0.1',
    packages=find_packages(exclude='tests*'),
    package_dir={'': 'src'},
    url='',
    license='',
    author="Michael Helmling",
    author_email='michaelhelmling@posteo.de',
    description='',
    entry_points={
        'console_scripts': [
            'omg-maestro-import = omg.apps.maestro_importer.script:run',
        ]
    },
)
