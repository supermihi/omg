import * as React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { AppContainer } from 'react-hot-loader';
import { connection } from 'actions/wamp';
import App from 'components/App';
import configureStore from 'configureStore';


const store = configureStore();
store.dispatch(connection.createConnectAction());

// tslint:disable-next-line:variable-name
const renderMain = (App: React.ReactType) => (
  <AppContainer>
    <Provider store={store}>
      <App />
    </Provider>
  </AppContainer>);

render(renderMain(App), document.getElementById('root'));

// Hot Module Replacement API
if (module.hot) {
  module.hot.accept('./components/App', () => {
    // tslint:disable-next-line:variable-name
    render(renderMain(require('./components/App').default),
      document.getElementById('root'));
  });
}
