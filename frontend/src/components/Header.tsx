import * as React from 'react';
import { Layout } from 'antd';
import styles from './styles.scss';

interface Props {
  info: string;
}

export default (props: Props) => <Layout.Header className={styles.header}>
  <p>{props.info}</p>
</Layout.Header>;
