import * as React from 'react';
import { Button, Modal, message } from 'antd';
interface Props {
  connected: boolean;
  error: string;
  info: string;
  connect: () => any;
  disconnect: () => any;
  getInfo: () => any;
}

export default class ConnectionManager extends React.Component<Props, {}> {

  componentDidUpdate() {
    const { error, connected, info, getInfo } = this.props;
    if (error) {
      message.error(error);
      return;
    }
    if (connected && !info) {
      getInfo();
    }
  }

  render() {
    const { connected, info, connect, disconnect } = this.props;
    return <Modal
      title="Connected to OMG server"
      footer={null}
      visible={true} >
      <p>{connected ? 'Fetching OMG info ...' : 'Not connected.'}</p>
      {connected || <Button onClick={connect}>connect</Button>}
    </Modal>;
  }    
}
