import * as React from 'react';

import styles from './styles.scss';

interface Props {
  children: React.ReactNode;
}
export default (props: Props) => (
  <div className={styles.centralWidget}>
    {React.Children.only(props.children)}
  </div>
);
