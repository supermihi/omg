import * as React from 'react';
import { Layout } from 'antd';

import Header from 'containers/Header';
import Search from 'Search/components';
import CentralWidget from './CentralWidget';
import styles from './styles.scss';

export default () => (
  <Layout className={styles.layout}>
    <Header />
    <Layout.Content className={styles.content}>
      <CentralWidget>
        <Search />
      </CentralWidget>  
    </Layout.Content>
  </Layout>
);
