import * as React from 'react';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';
import ConnectionConditional from 'containers/ConnectionConditional';
import ConnectionManager from 'containers/ConnectionManager';
import ConnectedApp from 'components/ConnectedApp';

export default class App extends React.Component {
  public render() {
    return (
      <LocaleProvider locale={enUS}>
        <ConnectionConditional connected={ConnectedApp} notConnected={ConnectionManager} />
      </LocaleProvider>
    );
  }
}
