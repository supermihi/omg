import * as React from 'react';
import Element from '../Element';

export default class ElementList extends React.PureComponent<{allElements: Element[]}, {}> {

    public render() {
        const elems = this.props.allElements;
        return <ul> {elems ? elems.map((el) => <li key={el.id}>{el.tags.title[0]}</li>) : null} </ul>;
    }
}
