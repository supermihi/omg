import { Action } from 'redux-actions';
import { combineReducers } from 'redux';
import connection, { ConnectionState } from './connection';
import search, { SearchState } from 'Search/reducers';

export interface State {
  connection: ConnectionState;
  search: SearchState;
}

export default combineReducers({
  connection,
  search,
});
