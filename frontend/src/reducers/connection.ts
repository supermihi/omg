import { connection } from 'actions/wamp';
import { getInfo } from 'actions/omg';
import { handleActions } from 'redux-actions';

export type ConnectionState = {
  calling: boolean;
  connected: boolean;
  info: string;
  error?: string;
};

const initialState: ConnectionState = { connected: false, info: null, calling: false };

export default handleActions({
  [connection.type]: connection.createReducer(
    (state, payload) => ({ ...state, calling: false, connected: true }),
    state => ({ ...state, calling: false, connected: false }),
    state => ({ ...state, calling: true, error: undefined }),
    state => ({ ...state, calling: true, error: undefined }),
    (state, payload) => ({ ...state, calling: false, connected: false, error: payload })),
  [getInfo.type]: getInfo.createReducer(
    (state, _, response) => ({ ...state, info: response }),
    state => ({ ...state, calling: true }),
    (state, _, error) => ({ ...state, error, calling: false }),
  ),
}
  , initialState);
