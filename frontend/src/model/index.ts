export interface TagAndValue {
  tag: string;
  value: string;
}

export type Tags = { [tag: string]: string[] };

export interface Element {
  id: string;
  tags: Tags;
  flags: string[];
  url: string;
  sync: string;  
}

export interface Container extends Element {
  children: string[];
}

export interface File extends Element {
  fingerprint: string;
}
