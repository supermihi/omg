import { ReduxWampCaller } from 'redux-wamp';

export const getInfo = new ReduxWampCaller<string>('GET_OMG_INFO', 'omg.get_info');

