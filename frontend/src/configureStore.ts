import { createStore, applyMiddleware, compose } from 'redux';
import ReduxThunk from 'redux-thunk';
import { ReduxWamp } from 'redux-wamp';
import reducers from './reducers';

const devtoolsCompose = ((compose: any) => (
  compose ? compose({ shouldCatchErrors: true })
    : undefined))((window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__);

const composeEnhancers: typeof compose =
  process.env.NODE_ENV !== 'production' && devtoolsCompose || compose;



export default function configureStore() {
  const wamp = new ReduxWamp();
  const enhancer = composeEnhancers(applyMiddleware(ReduxThunk.withExtraArgument(wamp)),
    applyMiddleware(wamp.createMiddleware()));
  const store = createStore(reducers, {}, enhancer);
  if (module.hot) {
    module.hot.accept('./reducers', () => store.replaceReducer(require('./reducers').default));
  }
  return store;
}
