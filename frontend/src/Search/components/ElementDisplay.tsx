import * as React from 'react';
import { Icon } from 'antd';
import { ElementViewModel } from './ElementList';

interface Props {
  element: ElementViewModel;
}

export default function elementDisplay(props: Props) {
  const { element, expanded } = props.element;
  return <div key={element.id} >
    <Icon type="down-circle" />
    <strong>{element.tags['title'][0]}</strong>
  </div>;
}
