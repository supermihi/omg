import * as React from 'react';
import SearchInput from '../containers/Input';
import FilteredElements from '../containers/FilteredElements';
import TagFilters from '../containers/TagFilters';

export default () => <div>
  <SearchInput />
  <TagFilters />
  <FilteredElements />

</div>;
