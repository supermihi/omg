import * as React from 'react';
import { AutoComplete, Input, Select } from 'antd';
import styles from './styles.scss';
import { TagAndValue } from 'model';
import { OptionProps } from 'antd/lib/select';
const { Option, OptGroup } = Select;

interface Props {
  value: string;
  matchedTags: {[token: string]: TagAndValue[] };
  setSearchString: (s: string) => any;
  matchTags: (s: string) => any;
  addTagFilter: (tag: TagAndValue) => any;
  findElementsCurrentFilter: () => any;
}

function makeString(token: string, tag: TagAndValue) {
  return `${token}///${tag.tag}///${tag.value}`;
}

function parseString(s: string) {
  const split = s.split('///');
  return {
    token: split[0],
    tag: split[1],
    value: split[2],
  };
}

export default class SearchInput extends React.Component<Props> {

  constructor(props: Props) {
    super(props);
    this.updateCurrentOptions(props.matchedTags);
    this.timeout = 0;
  }
  private currentOptions: React.ReactNode[];
  private timeout: number;
  
  private updateCurrentOptions(matchedTags: {[token: string]: TagAndValue[]}) {
    this.currentOptions = matchedTags ?
      [
        ...Object.keys(matchedTags).sort().map(token =>
          <OptGroup
            key={token}
            label={<span style={{ fontWeight: 'bold' }}>{token}</span>}>
            {matchedTags[token].map(t => (
              <Option key={`${token}-${t.tag}-${t.value}`} value={makeString(token, t)}>
                {`${t.tag}: ${t.value}`}
              </Option>))}
          </OptGroup>),
      ]
      : [];
  }

  componentWillReceiveProps(nextProps: Props) {
    if (nextProps.matchedTags !== this.props.matchedTags) {
      this.updateCurrentOptions(nextProps.matchedTags);
    }
  }

  private handleSearch(s: string) {
    const { setSearchString, matchTags } = this.props;
    setSearchString(s);
    if (this.timeout) {
      clearTimeout(this.timeout);
    }
    if (s) {
      this.timeout = setTimeout(() => matchTags(s), 250) as any;
    }  
  }

  private handleChange(value: any) {
    console.log(`change value: ${value}`);
  }

  private handleSelect(s: string) {
    const { token, tag, value } = parseString(s);
    this.props.addTagFilter({ tag, value });
    this.props.findElementsCurrentFilter();
    this.props.setSearchString('');
  }

  render() {
    return <AutoComplete style={{ width: '100%' }}
      dataSource={this.currentOptions as any}
      onSearch={s => this.handleSearch(s)}
      filterOption={false}
      optionLabelProp={'children'}
      onChange={v => this.handleChange(v)}
      onSelect={v => this.handleSelect(v as string)}
      value={this.props.value}>
      <Input.Search className={styles.input} />
    </AutoComplete>;
  }

}
