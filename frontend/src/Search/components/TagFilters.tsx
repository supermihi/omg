import * as React from 'react';
import { Tag } from 'antd';
import { TagAndValue } from 'model';

interface Props {
  filters: TagAndValue[];
  removeTagFilter: (tag: TagAndValue) => any;
}

export default function tagFilter({ filters, removeTagFilter }: Props) {
  return <div>
    {filters.map(tag => (
      <Tag closable onClose={() => removeTagFilter(tag)} key={`${tag.tag}-${tag.value}`}>
        {tag.tag}: {tag.value}  
      </Tag>
    ))}
  </div>;
}
