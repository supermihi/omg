import * as React from 'react';
import { Element } from 'model';
import ElementList from 'Search/components/ElementList';

interface Props {
  elements: Element[];
}
export default function filteredElements({ elements }: Props) {
  return <ElementList elements={elements.map(element => ({ element, expanded: false }))} />;
}
