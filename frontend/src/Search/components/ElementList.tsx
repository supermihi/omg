import * as React from 'react';
import { Element } from 'model';
import ElementDisplay from './ElementDisplay';
import styles from './styles.scss';

export interface ElementViewModel {
  element: Element;
  expanded: boolean;
}

interface Props {
  elements: ElementViewModel[];
}

export default function elementList(props: Props) {
  return <div className={styles.elementList}>
    {props.elements.map(e => <ElementDisplay element={e} />)}
  </div>;
}
