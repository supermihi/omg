import { handleActions } from 'redux-actions';
import { isType } from 'typescript-fsa';
import { Action } from 'redux';
import { setSearchString, matchTags, addTagFilter, removeTagFilter, findElements } from '../actions';
import { TagAndValue, Element } from 'model';
import { List } from 'immutable';

export interface SearchState {
  searchString: string;
  matchedTags: {[token: string]: TagAndValue[] };
  tagFilters: TagAndValue[];
  elements: Element[];
}

const initialState: SearchState = { searchString: '', matchedTags: null, tagFilters: [], elements: [] };

const matchTagsReducer = matchTags.createReducer<SearchState>(
  (state, args, result) => ({ ...state, matchedTags: { [state.searchString]: result } }),
);

const findElementsReducer = findElements.createReducer<SearchState>(
  (state, args, elements) => ({ ...state, elements }),
);

export default function reducer(state: SearchState, action: Action) {
  if (isType(action, setSearchString)) {
    const { payload } = action;
    return { ...state, searchString: payload, matchedTags: payload ? state.matchedTags : null };
  }
  if (matchTags.isCallAction(action)) {
    return matchTagsReducer(state, action);
  }
  if (findElements.isCallAction(action)) {
    return findElementsReducer(state, action);
  }
  if (isType(action, addTagFilter)) {
    return { ...state, tagFilters: [...state.tagFilters, action.payload] };
  }
  if (isType(action, removeTagFilter)) {
    return {
      ...state, tagFilters:
        state.tagFilters.filter(({ tag, value }) => !(tag === action.payload.tag && value === action.payload.value)),
    };
  }
  return state || initialState;
}
