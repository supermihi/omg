import actionCreatorFactory from 'typescript-fsa';
import { TagAndValue, Element } from 'model';
import ReduxWampCaller from 'redux-wamp/dist/roles/call';
import { Dispatch } from 'redux';
import { State } from 'reducers';

const actionCreator = actionCreatorFactory();

export const setSearchString = actionCreator<string>('SET_SEARCH_STRING');
export const addTagFilter = actionCreator<TagAndValue>('ADD_TAG_FILTER');
export const removeTagFilter = actionCreator<TagAndValue>('REMOVE_TAG_FILTER');

export const matchTags = new ReduxWampCaller<TagAndValue[], [string, string]>('MATCH_TAGS', 'omg.match_tags');
export const findElements = new ReduxWampCaller<Element[], [TagAndValue[][]]>('FIND_ELEMENTS', 'omg.find_elements');

export const findElementsCurrentFilter = () => (dispatch: Dispatch<any>, getState: () => State) => {
  const state = getState();
  const tagFilters = state.search.tagFilters;
  dispatch(findElements.createCallAction([tagFilters.map(e => [e])]));
};
