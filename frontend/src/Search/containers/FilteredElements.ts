import { connect } from 'react-redux';
import { State } from 'reducers';
import FilteredElements from '../components/FilteredElements';

const mapStateToProps = (state: State) => ({ elements: state.search.elements });

export default connect(mapStateToProps)(FilteredElements);
