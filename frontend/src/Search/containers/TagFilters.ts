import { connect } from 'react-redux';
import { removeTagFilter } from '../actions';
import { State } from 'reducers';
import TagFilters from '../components/TagFilters';

const mapStateToProps = (state: State) => ({ filters: state.search.tagFilters });
const mapDispatchToProps = { removeTagFilter };

export default connect(mapStateToProps, mapDispatchToProps)(TagFilters);
