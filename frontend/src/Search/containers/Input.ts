import { connect, Dispatch } from 'react-redux';
import { setSearchString, matchTags, addTagFilter, findElementsCurrentFilter } from '../actions';
import { State } from 'reducers';
import SearchInput from '../components/Input';
import { TagAndValue } from 'model';

const mapStateToProps = ({ search }: State) => ({
  value: search.searchString,
  matchedTags: search.matchedTags,
});

const mapDispatchToProps = {
  findElementsCurrentFilter,
  addTagFilter: (t: TagAndValue) => addTagFilter(t),
  setSearchString: (s: string) => setSearchString(s),  
  matchTags: (value: string, tag?: string) => matchTags.createCallAction([value, tag]),
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchInput);
