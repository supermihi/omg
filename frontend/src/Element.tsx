
interface IRawElement {
    _id: {$uuid: string};
    _cls: string;
}

interface Tag {
  tag: string;
  values: string[];
}

interface TagDict {
  [key: string]: string[];
}

function makeTags(tags: Tag[]) {
  const result: TagDict = {};
  for (const tag of tags) {
    result[tag.tag] = tag.values;
  }
  return result;
}

export default class Element {
    public readonly id: string;
    public readonly cls: string;
    public readonly tags: any;

    public constructor({_id, _cls, tags, ...rest}: any) {
        this.id = _id.$uuid;
        this.cls = _cls;
        this.tags = makeTags(tags);
        Object.assign(this, rest);
    }
}