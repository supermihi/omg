import { connect } from 'react-redux';
import { connection } from 'actions/wamp';
import { getInfo } from 'actions/omg';
import { State } from 'reducers';
import ConnectionManager from 'components/ConnectionManager';

const mapStateToProps = ({ connection: { connected, error, info } }: State) => ({
  connected,
  error,
  info,
});

const mapDispatchToProps = {
  connect: connection.createConnectAction,
  disconnect: connection.createDisconnectAction,
  getInfo: getInfo.createCallAction,
};

export default connect(mapStateToProps, mapDispatchToProps)(ConnectionManager);
