import * as React from 'react';
import { connect } from 'react-redux';

import { State } from 'reducers';

interface OwnProps {
  connected: React.ComponentType;
  notConnected: React.ComponentType;
}

interface StateProps {
  isConnected: boolean;
}

type Props = OwnProps & StateProps;

class ConnectionConditional extends React.Component<Props> {

  render() {
    const { connected, notConnected, isConnected } = this.props;
    return isConnected
      ? <this.props.connected />
      : <this.props.notConnected />;
  }
}

const mapStateToProps = (state: State, ownProps: OwnProps) => ({
  isConnected: state.connection.connected,
  ...ownProps,
});

export default connect(mapStateToProps)(ConnectionConditional);