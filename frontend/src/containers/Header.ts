import { connect } from 'react-redux';
import { State } from 'reducers';
import Header from 'components/Header';

const mapStateToProps = (s: State) => ({ info: s.connection.info });

export default connect(mapStateToProps)(Header);
